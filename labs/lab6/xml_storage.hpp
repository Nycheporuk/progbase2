#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "optional.hpp"
#include "book.hpp"
#include "author.hpp"
#include "storage.hpp"

#include "QString"
#include "QFile"
#include "QDebug"
#include "QtXml"

using std::string;
using std::vector;

class XmlStorage : public Storage
{
    const string dir_name_;

    vector<Book> books_;
    vector<Author> authors_;
    int getNewBookId(string dir_name);
    int getNewAuthorId(string dir_name);

    void clearIdFile();
    void deleteAll();

public:
    XmlStorage(const string & dir_name) : dir_name_(dir_name) { }

    bool load();
    bool save();

    // books
    vector<Book> getAllBooks();
    optional<Book> getBookById(int book_id);
    bool updateBook(const Book & book);
    bool removeBook(int book_id);
    int insertBook(const Book & book);
    // authors
    vector<Author> getAllAuthors(void);
    optional<Author> getAuthorById(int author_id);
    bool updateAuthor(const Author &author);
    bool removeAuthor(int author_id);
    int insertAuthor(const Author &author);
};
