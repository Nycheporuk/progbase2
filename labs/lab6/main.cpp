#include <fstream>
#include <iostream>
#include <istream>

#include "csv_storage.hpp"
#include "xml_storage.hpp"
#include "storage.hpp"
#include "cui.hpp"
#include "iostream"

using namespace std;

int main()
{
    cout << "Enter data type: ";
    string data_type;
    cin >> data_type;
    if (strcmp(data_type.c_str(), "xml") == 0)
    {
        XmlStorage xml_storage("../lab6/data/xml/");
        Storage * storage = &xml_storage;

        if (!storage->load())
        {
            cerr << "Can't load storage" << endl;
            abort();
        }

        Cui cui(storage);
        cui.show();
    }
    else if (strcmp(data_type.c_str(), "csv") == 0)
    {
        CsvStorage csv_storage("../lab6/data/csv/");
        Storage * storage = &csv_storage;

        if (!storagePtr->load())
        {
            cerr << "Can't load storage" << endl;
            abort();
        }

        Cui cui(storage);
        cui.show();
    }
    else
    {
        cout << "data type doesn't exist" << endl;
    }

    return 0;
}
