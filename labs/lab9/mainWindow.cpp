#include "mainWindow.h"
#include "ui_mainWindow.h"

#include <QPixmap>
#include <QListWidget>
#include <QDebug>
#include <QFileDialog>
#include <QString>
#include <QDialog>
#include <QtGui>
#include <string.h>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include "sqlite_storage.h"
#include <QFile>
#include "csv.hpp"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


}

MainWindow::~MainWindow()
{
    delete storage_;
    delete ui;
}

void MainWindow::on_AddButton_clicked()
{
    addDialog = new AddDialog(this);
    addDialog->show();

    connect(addDialog, SIGNAL(sendBook(Book*)), this, SLOT(getBook(Book*)));
    //MainWindow::updatePage();
}

void MainWindow::on_EditButton_clicked()
{
    editDialog = new EditDialog(this);
    editDialog->show();

    connect(this, SIGNAL(sendBookToEdit(QListWidgetItem*)), editDialog, SLOT(getBookToEdit(QListWidgetItem*)));
    connect(editDialog, SIGNAL(sendUpdatedBook(Book*)), this, SLOT(getUpdatedBook(Book*)));

    QListWidgetItem * item = ui->listWidget->selectedItems().at(0);
    emit(sendBookToEdit(item));
}

void MainWindow::on_RemoveButton_clicked()
{
    vector<Book> book = storage_->getAllBooks();
    if (book.empty())
        return;

    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(
        this,
        "Delete",
        "Are you sure?",
        QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
        return;

    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();

    if (items.at(0) == NULL)
        return;

    foreach (QListWidgetItem * item, items)
    {
        items = ui->listWidget->selectedItems();
        QVariant var = item->data(Qt::UserRole);
        Book bk = var.value<Book>();
        int index = bk.id_;
        qDebug() << "index: " << index;
        delete ui->listWidget->takeItem(ui->listWidget->row(item));

        if(!storage_->removeBook(index))
            qDebug() << "Can't delete";
    }

    book = storage_->getAllBooks();

    if (book.empty())
    {
        ui->RemoveButton->setEnabled(false);
        ui->EditButton->setEnabled(false);

        ui->nameFillLabel->setText("-");
        ui->authorFillLabel->setText("-");
        ui->chaptersFillLabel->setText("-");

    }
    else
    {
        items = ui->listWidget->selectedItems();
        QListWidgetItem * item = items.at(0);
        QVariant var = item->data(Qt::UserRole);
        Book bk = var.value<Book>();

        ui->nameFillLabel->setText(QString::fromStdString(bk.name_));
        ui->authorFillLabel->setText(QString::fromStdString(bk.author_));
        ui->chaptersFillLabel->setText(QString::fromStdString(to_string(bk.chapters_)));

    }


    return;
}
void MainWindow::getBook(Book * book)
{
    storage_->insertBook(*book, user_.id);
    vector<Book> books = storage_->getAllBooks();

    QListWidget * ListWidget = ui->listWidget;
    QListWidgetItem * qBookListItem = new QListWidgetItem();

    QVariant qVariant;
    qVariant.setValue(books.at(books.size() - 1));

    QString name = QString::fromStdString(books.at(books.size() - 1).name_);
    qBookListItem->setText(name);
    qBookListItem->setData(Qt::UserRole, qVariant);

    ListWidget->addItem(qBookListItem);
}
void MainWindow::getUpdatedBook(Book * book)
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    QVariant var = item->data(Qt::UserRole);
    Book bk = var.value<Book>();
    book->id_ = bk.id_;

    if(!storage_->updateBook(*book))
    {
        qDebug() << "Can't update";
        return;
    }
    else
    {
        qDebug() << "Updated";
    }

    QListWidgetItem * qBookListItem = ui->listWidget->takeItem(ui->listWidget->row(item));

    QVariant qVariant;
    qVariant.setValue(*book);

    QString name = QString::fromStdString(book->name_);
    qBookListItem->setText(name);
    qBookListItem->setData(Qt::UserRole, qVariant);

    QListWidget * ListWidget = ui->listWidget;
    ListWidget->addItem(qBookListItem);
}

void MainWindow::loadBooks(QString file_path)
{
    file_name = file_path;
    SqliteStorage * sql_storage = new SqliteStorage(file_name.toStdString());
    storage_ = sql_storage;

    if(!storage_->open())
    {
        qDebug() << "cant open storage";

        return;
    }

    vector<Book> books = storage_->getAllUserBooks(user_.id);

    for (size_t i = 0; i < books.size(); i++)
    {
        QListWidget * ListWidget = ui->listWidget;
        QListWidgetItem * qBookListItem = new QListWidgetItem();

        QVariant qVariant;
        qVariant.setValue(books.at(i));

        QString name = QString::fromStdString(books.at(i).name_);
        qBookListItem->setText(name);
        qBookListItem->setData(Qt::UserRole, qVariant);

        ListWidget->addItem(qBookListItem);
    }


    ui->listWidget->setEnabled(true);
    ui->nameFillLabel->setEnabled(true);
    ui->authorFillLabel->setEnabled(true);
    ui->chaptersFillLabel->setEnabled(true);
    ui->AddButton->setEnabled(true);
    ui->EditButton->setEnabled(false);
    ui->RemoveButton->setEnabled(false);
    ui->nameLabel->setEnabled(true);
    ui->authorLabel->setEnabled(true);
    ui->chaptersLabel->setEnabled(true);
    ui->label->setEnabled(true);

}
void MainWindow::on_listWidget_itemClicked (QListWidgetItem *book)
{
    ui->EditButton->setEnabled(true);
    ui->RemoveButton->setEnabled(true);
    ui->AuthorButton->setEnabled(true);

    QVariant var = book->data(Qt::UserRole);
    Book bk = var.value<Book>();
    qDebug() << bk.id_;
    book_id_ = bk.id_;
    ui->nameFillLabel->setText(QString::fromStdString(bk.name_));
    ui->authorFillLabel->setText(QString::fromStdString(bk.author_));
    ui->chaptersFillLabel->setText(QString::fromStdString(to_string(bk.chapters_)));
    QString path =  QString::fromStdString(bk.image_path);
    if(path.isEmpty() || path.isNull()){
        ui->Image->clear();
    }
    else{

        QPixmap myPixmap(path );

        ui->Image->setPixmap( myPixmap );

    }
}

void MainWindow::on_AuthorButton_clicked() {
    authorWindow = new AuthorWindow(this);
    authorWindow->show();

    connect(this, SIGNAL(sendUserIdToPF(int)), authorWindow, SLOT(getUserId(int)));
    connect(this, SIGNAL(sendBookIdToPF(int)), authorWindow, SLOT(getBookId(int)));

    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    QVariant var = item->data(Qt::UserRole);
    Book book = var.value<Book>();

    emit(sendUserIdToPF(user_.id));
    emit(sendBookIdToPF(book.id_));

    authorWindow->load(file_name);
}
void MainWindow::getUser(User user)
{
    user_.id = user.id;
    user_.password_hash = user.password_hash;
    user_.username = user.username;
}

void MainWindow::on_actionExit_triggered()
{
    MainWindow::close();
}

void MainWindow::on_SearchLine_textChanged(const QString &arg1)
{
    int var = 0;
   vector<Book> books;
    books = storage_->getAllUserBooks(user_.id);
    ui->listWidget->clear();
    for(size_t i = 0; i < books.size(); i++)
    {
        var = 0;
        Book book = books.at(i);
        QString name = QString::fromStdString(book.name_);
        if(arg1.size() == 0)
        {
            ui->listWidget->addItem(name);
        }
        if(arg1.size() > name.size())
        {
            continue;
        }
        else if(arg1.size() <= book.name_.size())
        {
            for(size_t j = 0; j < arg1.size(); j++)
            {
                    if(name.at(j) != arg1.at(j))
                    {

                        var = 1;
                    }
                    else if(var != 1)
                    {
                        var = 2;

                    }
            }
            if(var == 2)
            {
                ui->listWidget->addItem(name);
            }
        }

    }

}



void MainWindow::on_CSV_clicked()
{
    QFile file;
    QString name = ui->CSVname->text();
    QString filename = "../course/data/csv/" + name;
    if(name.isEmpty() || name.isNull())
    {
        name = "data.csv";
        filename = "../course/data/csv/data.csv";
    }
    vector<Book> books = storage_->getAllUserBooks(user_.id);
    QString text = QString::fromStdString(Csv::createStringFromBooks(books));
    file.setFileName(filename);

        if (!file.open(QFile::WriteOnly))
        {
            qDebug() << "Error. Can not open " << filename << " for writing" << endl;
            return ;
        }

        QTextStream qts(&file);
        qts << text;
        file.close();
       ui->CSVname->setText("");
       qDebug() << "Successfully created" << name;
}
