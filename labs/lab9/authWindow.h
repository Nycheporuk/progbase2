 #ifndef AUTHWINDOW_H
#define AUTHWINDOW_H

#pragma once

#include <QDialog>
#include <QMainWindow>
#include <QListWidget>
#include <QMessageBox>

#include "user.h"
#include "mainWindow.h"
#include "registrationwindow.h"

namespace Ui {
class AuthWindow;
}

class AuthWindow : public QDialog
{
    Q_OBJECT

public:
    explicit AuthWindow(QWidget *parent = 0);
    ~AuthWindow();

signals:
    void sendUser(User);

private slots:
    void on_button_ok_clicked();

    void on_button_cancel_clicked();

    void on_RegisterButton_clicked();

private:
    Ui::AuthWindow *ui;
    std::string dir_name_;
    MainWindow * main_window;
    RegistrationWindow * registr_window;
};

#endif // AUTHWINDOW_H
