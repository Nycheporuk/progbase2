#pragma once
#include <vector>
#include <string>
#include "book.hpp"
using std::string;
using std::vector;

using CsvRow = vector<string>;  
using CsvTable = vector<CsvRow>; 

namespace Csv
{
CsvTable createTableFromString(const string &csvStr); 
string createStringFromTable(const CsvTable &csvTable);
string createStringFromBooks(const vector<Book> & books);

} 
