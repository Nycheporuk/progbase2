#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>
#include <QMainWindow>
#include <QListWidget>
#include <QMessageBox>

#include "book.hpp"

namespace Ui {
class AddDialog;
}

class AddDialog : public QDialog
{
    Q_OBJECT
signals:
    void sendBook(Book*);

public:
    explicit AddDialog(QWidget *parent = 0);
    ~AddDialog();


private slots:
    void on_Cansel_Ok_accepted();

    void on_Cansel_Ok_rejected();

    void on_ImageButton_clicked();

private:
    Ui::AddDialog *ui;
    QString path;
};

#endif // ADDDIALOG_H
