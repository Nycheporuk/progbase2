
#pragma once
#include <string>
#include <QMetaType>

using namespace std;

class Author
{
public:
    int id_ = 0;
    string name_;
    string country_;
    int year_;
};

Q_DECLARE_METATYPE(Author)
