#ifndef AUTHORWINDOW_H
#define AUTHORWINDOW_H

#include <QMainWindow>
#include <QDialog>
#include <QListWidget>
#include <QListWidgetItem>
#include <QMessageBox>

#include "author.hpp"
#include "storage.hpp"
#include "sqlite_storage.h"
#include "addAuthorDialog.h"
#include "editAuthorDialog.h"

namespace Ui {
class AuthorWindow;
}

class AuthorWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit AuthorWindow(QWidget *parent = 0);
    ~AuthorWindow();

    void load(QString file_name);

signals:
    void sendAuthorToEditForm(QListWidgetItem*);

private slots:
    void on_AddButton_clicked();

    void on_CloseButton_clicked();

    void on_RemoveButton_clicked();

    void on_EditButton_clicked();

    void getUserId(int user_id);

    void getBookId(int book_id);

    void getAuthor(Author * author);

    void getUpdAuthor(Author author);

    void on_listWidget_itemClicked(QListWidgetItem * author);

    void getAuthorsToDelete(vector<int>);

private:
    Ui::AuthorWindow *ui;

    Storage * storage_ = nullptr;
    AddAuthorDialog * addDialog;
    EditAuthorDialog * editDialog;
    QString file_name_;
    int user_id_;
    int book_id_;

};

#endif // AUTHORWINDOW_H
