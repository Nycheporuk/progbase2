#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H

#pragma once

#include <QtSql>
#include <QVariant>

#include "storage.hpp"
#include "book.hpp"
#include "author.hpp"
#include "user.h"

class SqliteStorage : public Storage
{
    QSqlDatabase db_;
    string dir_name_;

public:
    SqliteStorage(const string & dir_name);

    bool open();
    bool close();

    // books
    vector<Book> getAllUserBooks(int user_id); //new DONE
    vector<Book> getAllBooks(void);
    optional<Book> getBookById(int book_id);
    bool updateBook(const Book & book);
    bool removeBook(int book_id);
    int insertBook(const Book & book);
    int insertBook(const Book & book, int user_id);

    // paintings

    optional<Author> getAuthorById(int author_id);


    int insertAuthor(const Author & author, int user_id);
    bool removeAuthor(int author_id, int user_id);
    bool updateAuthor(const Author & author);
    vector<Author> getAllUserAuthors(int user_id);

    // users
    optional<User> getUserAuth(string & username, string & password);//new DONE

    // links
    vector<Author> getAllBookAuthors(int book_id, int user_id);
    bool insertBookAuthor(int book_id, int author_id, int user_id);//new DONE
    bool removeBookAuthor(int book_id, int author_id, int user_id);//new DONE
    bool removeByBookId(int book_id, int user_id);
    bool removeByAuthorId(int author_id, int user_id);
    bool addUser(QString & username, QString & password);
    vector<Book> getPageOfBooks(int user_id, int current_page, int page_size);
 };

#endif // SQLITE_STORAGE_H
