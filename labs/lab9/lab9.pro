#-------------------------------------------------
#
# Project created by QtCreator 2019-05-17T21:00:41
#
#-------------------------------------------------



QT       += core gui sql
CONFIG += c++14 console


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
DEFINES += QT_DEPRECATED_WARNINGS



TARGET = lab9
TEMPLATE = app
SOURCES += \
    main.cpp\
    mainWindow.cpp \
    addDialog.cpp \
    editDialog.cpp \
    addAuthorDialog.cpp \
    editAuthorDialog.cpp\
    authorWindow.cpp \
    authWindow.cpp \
    registrationwindow.cpp\
    author.cpp \
    book.cpp \
    csv_storage.cpp \
    csv.cpp \
    generatorId.cpp \
    sqlite_storage.cpp \
    storage.cpp

HEADERS  += mainWindow.h \
    addDialog.h \
    editDialog.h\
    authorWindow.h \
    addAuthorDialog.h \
    editAuthorDialog.h\
    authWindow.h \
    registrationwindow.h \

    author.hpp \
    book.hpp \
    csv_storage.hpp \
    csv.hpp \
    generatorId.hpp \
    sqlite_storage.h \
    storage.hpp \
    user.h


FORMS    += mainWindow.ui \
    addDialog.ui \
    editDialog.ui \
    addAuthorDialog.ui \
    editAuthorDialog.ui \
    authorWindow.ui \
    authWindow.ui \
    registrationwindow.ui

DISTFILES += \
    data/csv/books.csv\
    data/sql/data.sqlite

