#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "authorWindow.h"


#include <QListWidget>
#include <QDebug>
#include <QFileDialog>
#include <QString>
#include <QDialog>
#include <QtGui>
#include <string.h>
#include <fstream>
#include <sstream>
#include <stdio.h>

#include "addDialog.h"
#include "book.hpp"
#include "storage.hpp"
#include "generatorId.hpp"
#include "ui_mainWindow.h"
#include "csv_storage.hpp"
#include "editDialog.h"
#include "user.h"



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void loadBooks(QString file_path);

signals:
    void sendBookToEdit(QListWidgetItem*);
    void sendUserIdToPF(int user_id);
    void sendBookIdToPF(int book_id);
    void removeAuthorsOfBook(vector<int>);

private slots:
    void on_AddButton_clicked();

    void on_EditButton_clicked();

    void on_RemoveButton_clicked();

    void getBook(Book * book);

    void getUpdatedBook(Book * book);

    void getUser(User user);

    void on_listWidget_itemClicked(QListWidgetItem *item);


    void on_AuthorButton_clicked();

    void on_actionExit_triggered();

//    void on_PrevButton_clicked();

//    void on_NextButton_clicked();
//    void updatePage();

    void on_SearchLine_textChanged(const QString &arg1);


    void on_CSV_clicked();

private:
    Ui::MainWindow *ui;
    AddDialog * addDialog;
    EditDialog * editDialog;
    AuthorWindow * authorWindow;

    Storage * storage_ = nullptr;
    QString file_name;
    User user_;
    int book_id_;
};


#endif // MAINWINDOW_H
