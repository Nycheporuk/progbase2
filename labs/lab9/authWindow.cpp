#include "authWindow.h"
#include "ui_authWindow.h"
#include "mainWindow.h"
AuthWindow::AuthWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AuthWindow)
{
    ui->setupUi(this);
}

AuthWindow::~AuthWindow()
{
    delete ui;
}

QString hashPasswordAuthWindow(QString const & pass)
{
   QByteArray pass_ba = pass.toUtf8();
   QByteArray hash_ba = QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
   QString pass_hash = QString(hash_ba.toHex());
   return pass_hash;
}

void AuthWindow::on_button_ok_clicked()
{

    dir_name_ = "/home/nycheporuk/Programming/progbase3/course/data/sql/data.sqlite";
    SqliteStorage * sql_storage = new SqliteStorage(dir_name_);
    Storage * storage_ = sql_storage;
    if (!storage_->open())
    {
        qDebug() << "Can't open storage_";
    }

    string usrname = ui->usernameLine->text().toStdString();
    string password =  ui->passwordLine->text().toStdString();
    optional<User> user_opt = storage_->getUserAuth(usrname, password);
    qDebug() << "get User";


    if (user_opt != nullopt)
    {
        main_window = new MainWindow;
        User user_ = user_opt.value();
        User user;
//        user.id = user_id;
        user.id = user_.id;
        user.password_hash = hashPasswordAuthWindow(ui->passwordLine->text()).toStdString();
        user.username = ui->usernameLine->text().toStdString();
        connect(this, SIGNAL(sendUser(User)), main_window, SLOT(getUser(User)));
        emit(sendUser(user));
        main_window->loadBooks(QString::fromStdString(dir_name_));
        //storage_->close();
        delete storage_;
        this->close();
        main_window->show();
        return;
    }
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(
        this,
        "Wrong username or password",
        "Retry?",
        QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes)
    {
        ui->usernameLine->setText("");
        ui->passwordLine->setText("");
    }
    else
        this->close();
}

void AuthWindow::on_button_cancel_clicked()
{
    this->close();
}

void AuthWindow::on_RegisterButton_clicked()
{
    registr_window = new RegistrationWindow;
    registr_window->show();

}
