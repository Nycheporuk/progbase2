#include "registrationwindow.h"
#include "ui_registrationwindow.h"
#include <QMessageBox>
#include "user.h"
#include "storage.hpp"
#include <QDebug>
#include "storage.hpp"
#include "sqlite_storage.h"
RegistrationWindow::RegistrationWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RegistrationWindow)
{
    ui->setupUi(this);
}

RegistrationWindow::~RegistrationWindow()
{
    delete ui;
}

void RegistrationWindow::on_buttonBox_accepted()
{
    dir_name_ = "/home/nycheporuk/Programming/progbase3/course/data/sql/data.sqlite";
    SqliteStorage * sql_storage = new SqliteStorage(dir_name_);
    Storage * storage_ = sql_storage;
    if (!storage_->open())
    {
        qDebug() << "Can't open storage_";
    }
    QString password = ui->passwordLine->text();
    QString password2 = ui->passwordLine_2->text();
    QString username = ui->usernameLine->text();
    if(password.isEmpty() || password2.isEmpty() || username.isEmpty()){
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(
            this,
            "Empty username or password",
            "Retry?",
            QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes)
        {

        }
        else
            this->close();
     qDebug() << "field is empty";
      return;
    }
    if(password != password2){

        QMessageBox::StandardButton reply;
            reply = QMessageBox::question(
                this,
                "Password mistmatch",
                "Retry?",
                QMessageBox::Yes|QMessageBox::No);
            if (reply == QMessageBox::Yes)
            {
                ui->passwordLine->setText("");
                ui->passwordLine_2->setText("");
                return;
            }
            else
                this->close();
    }
    if(!storage_->addUser(username, password)){
        QMessageBox::StandardButton reply;
            reply = QMessageBox::question(
                this,
                "User already exists",
                "Retry?",
                QMessageBox::Yes|QMessageBox::No);
            if (reply == QMessageBox::Yes)
            {
                ui->usernameLine->setText("");
                ui->passwordLine->setText("");
                ui->passwordLine_2->setText("");


            }
            else
                this->close();
    }

    qDebug() << "user added" << username;
}

void RegistrationWindow::on_buttonBox_rejected()
{
    this->close();
}
