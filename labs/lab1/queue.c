#include"list.h"
#include"queue.h"

static void Queue_realloc(Queue *queue);

Queue *Queue_alloc(int cap)
{
    Queue *queue = malloc(sizeof(Queue) * cap);
    Queue_init(queue, cap);
    return queue;
}
void Queue_realloc(Queue *queue)
{
    int n = queue->capacity;
    queue->capacity *= 2;
    for(int i = n; i < queue->capacity; i++){
        char *str = realloc(queue->str[i], sizeof(char **) * queue->capacity);
        queue->str[i] = str;
    }
}
void Queue_init(Queue *queue, int cap)
{
    queue->str = (char **)malloc(sizeof (char *) * cap);
    queue->capacity = cap;
    queue->count = 0;
}
void Queue_deinit(Queue *queue)
{
    for (int i = 0; i < queue->count; i++)
    {
        free(queue->str[i]);
    }
    free(queue->str);
    free(queue);
}

size_t Queue_size(Queue *queue)
{
    return queue->count;
}
void Queue_enqueue(Queue *queue, char *value)
{
    if (queue->count >= queue->capacity)
    {
        Queue_realloc(queue);
    }

    queue->str[queue->count] = malloc(sizeof(char *) * strlen(value));
    strcpy(queue->str[queue->count], value);
    queue->count += 1;
}
char *Queue_dequeue(Queue *queue){
    char *buf = queue->str[0];
    for(int i = 0; i < Queue_size(queue) - 1; i++)
    {
        queue->str[i] = queue->str[i + 1];
    }
    queue->count -= 1;
    return buf;
}

void Queue_print(Queue * queue) 
{
    for(int i = 0; i < Queue_size(queue); i++) 
    {
        printf("%s\n", queue->str[i]);
    }
    printf("\n");
}
