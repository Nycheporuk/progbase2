#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <stdbool.h>
#include "list.h"
#include "queue.h"

void reorderList(List *self);
bool hasNoDigit(char *str);

void fillQueueFromList(List *self, Queue *queue1, Queue *queue2);
void fillListFromQueues(List *self, Queue *queue1, Queue *queue2);

void deInitStr(char * str);

void readFile(char *filename, List *self);