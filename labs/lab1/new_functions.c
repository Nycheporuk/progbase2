#include "new_functions.h"
#include"list.h"
#include"queue.h"


void fillQueueFromList(List *self, Queue *queue1, Queue *queue2){
    for (int i = 0; i < List_size(self); i++)
    {
        if ((i + 1) % 2 == 0)
        {
            Queue_enqueue(queue2, List_get(self, i));
        }
        else
        {
            Queue_enqueue(queue1, List_get(self, i));
        }
    }
}

void fillListFromQueues(List *self, Queue *queue1, Queue *queue2)
{
    puts("<list from queues> \n");
    
    for(int i = 0;  Queue_size(queue1) != 0; i++)  
    {
        List_add(self, Queue_dequeue(queue1));
    }
    for(int i = 0; Queue_size(queue2) != 0; i++) 
    {
        List_add(self, Queue_dequeue(queue2));
    }
}

bool hasNoDigit(char *str)
{
    for (int i = 0; i < strlen(str); i++)
    {
        if (isdigit(str[i]))
        {
            return false;
        }
    }
    return true;
}

void reorderList(List *self){
    puts("\n<Reorder list>\n");
    for(int i = 0; i < self->size; i++) 
    {
        if(hasNoDigit(List_get(self, i))) 
        {
            if(i == 0) 
            {
                continue;
            }  
            char * tmp = self->items[i];

            for(int j = i; j > 0; j--) 
            {
                self->items[j] = self->items[j - 1];
            }
            self->items[0] = tmp;
        }
    }
}
void deInitStr(char * str) 
{
    free(str);
}
void readFile(char *filename, List *self)
{
    char *buf = malloc(sizeof(char *) * 20);
    int count = 0;
    FILE *txt = fopen(filename, "r");
    char tmp = fgetc(txt);
    while (1)
    {
        if (tmp == '\n' || tmp == EOF)
        {
            buf[count] = '\0';
            List_add(self, buf);
            count = 0;
            if (tmp == EOF) 
            {
                break;
            }
        }
        else
        {
            buf[count] = tmp;
            count += 1;
        }

        tmp = fgetc(txt);
    }
    deInitStr(buf);
    fclose(txt);
}