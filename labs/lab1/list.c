#include "list.h"
#include "queue.h"

static void Queue_realloc(Queue *queue);
void List_init(List *self)
{
    self->capacity = 12;
    if ((self->items = malloc(sizeof(char *) * self->capacity)) == NULL)
    {
        printf("=error= can`t init\n");
        exit(1);
    }
    self->size = 0;
}
List *List_alloc()
{
    List *list;
    if ((list = malloc(sizeof(List))) == NULL)
    {
        printf("=error= can`t alloc\n");
        exit(1);
    }
    List_init(list);
    return list;
}
void List_realloc(List *self)
{
    puts("reallocl");
    self->capacity *= 2;
    self->items = realloc(self->items, sizeof(char *) * self->capacity);
}

void List_deinit(List *self)
{
    free(self->items);
}

void List_free(List *self)
{
    List_deinit(self);
    free(self);
}
char *List_get(List *self, int index)
{
    return self->items[index];
}
void List_set(List *self, int index, char *value)
{
    self->items[index] = value;
}
size_t List_size(List *self)
{
    return self->size;
}

void printList(List *self)
{
    for (int i = 0; i < self->size; i++)
    {
        
        printf("%s\n", List_get(self, i));
    }
    printf("\n");

}
void List_add(List *self, char *str)
{
    if (self->size >= self->capacity)
    {
        List_realloc(self);
    }

    self->items[self->size] = malloc(sizeof(char *) * strlen(str));
    strcpy(self->items[self->size], str);
    self->size += 1;

    
}