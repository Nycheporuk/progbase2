
#include "new_functions.h"

int main() 
{
    List * list = List_alloc();
    readFile("data.txt", list);
    puts(" ");
    printList(list);
    reorderList(list);
    printf("list>>>\n");
    printList(list);
    Queue * queue1 = Queue_alloc(6);
    Queue * queue2 = Queue_alloc(8);
    fillQueueFromList(list, queue1, queue2);
    printf("\nQueue1>>>\n\n");
    Queue_print(queue1);
    printf("\nQueue2>>>\n\n");
    Queue_print(queue2);
    List * list2 = List_alloc();
    fillListFromQueues(list2, queue1, queue2);
    printList(list2);
    Queue_deinit(queue1);
    Queue_deinit(queue2);
    List_deinit(list);
    List_deinit(list2);
    
    return 0;
}