#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <stdbool.h>

typedef struct List List;
struct List
{
    char **items;   
    size_t capacity; 
    size_t size;     
};
void List_init(List *self);  
void List_deinit(List *self);
List *List_alloc();          
void List_free(List *self); 


char *List_get(List *self, int index);
void List_set(List *self, int index, char *value);
void List_add(List *self, char *str);

void printList(List *self);

size_t List_size(List *self);


bool List_isEmpty(List *self);
void List_clear(List *self);