#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <stdbool.h>

typedef struct Queue Queue;

struct Queue
{
    char **str;
    int capacity;
    int count;
};

Queue *Queue_alloc(int cap);
void Queue_init(Queue *queue, int cap);
void Queue_deinit(Queue *queue);
size_t Queue_size(Queue *queue);
void Queue_enqueue(Queue *queue, char *value);
char *Queue_dequeue(Queue *queue);
bool Queue_isEmpty(Queue *queue);
void Queue_print(Queue *queue);
