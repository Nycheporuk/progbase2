#include "generatorId.hpp"

int generatorId::id_ = 0;

int generatorId::id_get()
{
    id_ += 1;
    return id_;
}

void generatorId::reset()
{
    id_ = 0;
}

