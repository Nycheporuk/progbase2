#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "optional.hpp"
#include "book.hpp"
#include "csv.hpp"
#include "storage.hpp"

using std::string;
using std::vector;

class CsvStorage : public Storage
{
    const string dir_name_;

    vector<Book> books_;

    Book rowToBook(const CsvRow & row);
    CsvRow BookToRow(const Book & book);

    void deleteAll();


public:
    CsvStorage(const string & dir_name) : dir_name_(dir_name) { }

    bool load();
    bool save();

    // books
    vector<Book> getAllBooks(void);
    optional<Book> getBookById(int id);
    bool updateBook(const Book & book);
    bool removeBook(int id);
    int insertBook(const Book & book);
};
