#pragma once
#include <string>
#include <QMetaType>

using namespace std;

class Book
{
public:
    Book();
    int id_ = 0;
    string name_;
    string author_;
    int chapters_;
};

Q_DECLARE_METATYPE(Book)
