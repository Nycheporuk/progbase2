#include "mainWindow.h"
#include "ui_mainWindow.h"

#include <QListWidget>
#include <QDebug>
#include <QFileDialog>
#include <QString>
#include <QDialog>
#include <QtGui>
#include <string.h>
#include <fstream>
#include <sstream>
#include <stdio.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_AddButton_clicked()
{
    addDialog = new AddDialog(this);
    addDialog->show();

    connect(addDialog, SIGNAL(sendBook(Book*)), this, SLOT(getBook(Book*)));
}

void MainWindow::on_EditButton_clicked()
{
    editDialog = new EditDialog(this);
    editDialog->show();

    connect(this, SIGNAL(sendBookToEdit(QListWidgetItem*)), editDialog, SLOT(getBookToEdit(QListWidgetItem*)));
    connect(editDialog, SIGNAL(sendUpdatedBook(Book*)), this, SLOT(getUpdatedBook(Book*)));

    QListWidgetItem * item = ui->listWidget->selectedItems().at(0);
    emit(sendBookToEdit(item));
}

void MainWindow::on_RemoveButton_clicked()
{
    vector<Book> book = storage_->getAllBooks();
    if (book.empty())
        return;

    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(
        this,
        "Delete",
        "Are you sure?",
        QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
        return;

    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();

    if (items.at(0) == NULL)
        return;

    foreach (QListWidgetItem * item, items)
    {
        items = ui->listWidget->selectedItems();
        QVariant var = item->data(Qt::UserRole);
        Book bk = var.value<Book>();
        int index = bk.id_;
        qDebug() << "index: " << index;
        delete ui->listWidget->takeItem(ui->listWidget->row(item));

        if(!storage_->removeBook(index))
            qDebug() << "Can't delete";
    }
    qDebug() << file_name << endl;

    if(!storage_->save())
        qDebug() << "Can't save in delete func" << endl;
    else
        qDebug() << "Saved in delete func" << endl;

    book = storage_->getAllBooks();

    if (book.empty())
    {
        ui->RemoveButton->setEnabled(false);
        ui->EditButton->setEnabled(false);

        ui->nameFillLabel->setText("-");
        ui->authorFillLabel->setText("-");
        ui->chaptersFillLabel->setText("-");

    }
    else
    {
        items = ui->listWidget->selectedItems();
        QListWidgetItem * item = items.at(0);
        QVariant var = item->data(Qt::UserRole);
        Book bk = var.value<Book>();

        ui->nameFillLabel->setText(QString::fromStdString(bk.name_));
        ui->authorFillLabel->setText(QString::fromStdString(bk.author_));
        ui->chaptersFillLabel->setText(QString::fromStdString(to_string(bk.chapters_)));

    }


    return;
}
void MainWindow::getBook(Book * book)
{
    storage_->insertBook(*book);
    vector<Book> books = storage_->getAllBooks();

    QListWidget * ListWidget = ui->listWidget;
    QListWidgetItem * qBookListItem = new QListWidgetItem();

    QVariant qVariant;
    qVariant.setValue(books.at(books.size() - 1));

    QString name = QString::fromStdString(books.at(books.size() - 1).name_);
    qBookListItem->setText(name);
    qBookListItem->setData(Qt::UserRole, qVariant);

    ListWidget->addItem(qBookListItem);
    storage_->save();
}

void MainWindow::getUpdatedBook(Book * book)
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    QVariant var = item->data(Qt::UserRole);
    Book bk = var.value<Book>();
    book->id_ = bk.id_;

    if(!storage_->updateBook(*book))
    {
        qDebug() << "Can't update";
        return;
    }
    else
    {
        qDebug() << "Updated";
    }
    storage_->save();

    QListWidgetItem * qBookListItem = ui->listWidget->takeItem(ui->listWidget->row(item));

    QVariant qVariant;
    qVariant.setValue(*book);

    QString name = QString::fromStdString(book->name_);
    qBookListItem->setText(name);
    qBookListItem->setData(Qt::UserRole, qVariant);

    QListWidget * ListWidget = ui->listWidget;
    ListWidget->addItem(qBookListItem);
}

void MainWindow::on_actionNew_triggered()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString current_dir = QDir::currentPath();
    QString default_name = "new_storage";
    QString folder_path = dialog.getSaveFileName(
        this,
        "Select New Storage Folder",
        current_dir + "/" + default_name + ".csv",
        "Folders");

    qDebug() << "Selected directory: " << folder_path;

    if (folder_path.size() == 0)
        return;
    ui->EditButton->setEnabled(true);
    ui->listWidget->setEnabled(true);
    ui->nameFillLabel->setEnabled(true);
    ui->authorFillLabel->setEnabled(true);
    ui->chaptersFillLabel->setEnabled(true);

    ui->AddButton->setEnabled(true);
    ui->EditButton->setEnabled(false);
    ui->RemoveButton->setEnabled(false);
    ui->nameLabel->setEnabled(true);
    ui->authorLabel->setEnabled(true);
    ui->chaptersLabel->setEnabled(true);
    ui->label->setEnabled(true);

    file_name = folder_path;

    CsvStorage * csv = new CsvStorage(folder_path.toStdString());
    storage_ = csv;
    storage_->save();
}


void MainWindow::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(
                this,
                "Dialog Caption",
                "",
                "CSV (*.csv);;All Files (*)");

    qDebug() << "Path:" << fileName;
    QDir dir(fileName);

    if (!dir.exists())
    {
        qDebug() << "Wrong path:" << fileName;
        dir.mkpath(".");
        fileName = fileName + "new_storage.csv";
        ofstream stor;
        stor.open(fileName.toStdString(), ios::out);
        stor.close();
    }
    file_name = fileName;

    CsvStorage * csv = new CsvStorage(fileName.toStdString());
    storage_ = csv;

    if(!storage_->load())
    {
        cerr << "Can't load storage: " << fileName.toStdString() << endl;
        cerr << "This file doesn't exist" << endl;
        return;
    }

    vector<Book> books = storage_->getAllBooks();

    for (size_t i = 0; i < books.size(); i++)
    {
        QListWidget * ListWidget = ui->listWidget;
        QListWidgetItem * qBookListItem = new QListWidgetItem();

        QVariant qVariant;
        qVariant.setValue(books.at(i));

        QString name = QString::fromStdString(books.at(i).name_);
        qBookListItem->setText(name);
        qBookListItem->setData(Qt::UserRole, qVariant);

        ListWidget->addItem(qBookListItem);
    }

    if(!storage_->save())
    {
        qDebug() << "Can't save" << endl;
    }
    else
        qDebug() << "Saved" << endl;

    ui->listWidget->setEnabled(true);
    ui->nameFillLabel->setEnabled(true);
    ui->authorFillLabel->setEnabled(true);
    ui->chaptersFillLabel->setEnabled(true);
    ui->AddButton->setEnabled(true);
    ui->EditButton->setEnabled(false);
    ui->RemoveButton->setEnabled(false);
    ui->nameLabel->setEnabled(true);
    ui->authorLabel->setEnabled(true);
    ui->chaptersLabel->setEnabled(true);
    ui->label->setEnabled(true);
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem *book)
{
    ui->EditButton->setEnabled(true);
    ui->RemoveButton->setEnabled(true);

    QVariant var = book->data(Qt::UserRole);
    Book bk = var.value<Book>();
    qDebug() << bk.id_;

    ui->nameFillLabel->setText(QString::fromStdString(bk.name_));
    ui->authorFillLabel->setText(QString::fromStdString(bk.author_));
    ui->chaptersFillLabel->setText(QString::fromStdString(to_string(bk.chapters_)));
}
