#-------------------------------------------------
#
# Project created by QtCreator 2019-05-17T21:00:41
#
#-------------------------------------------------

QT       += core gui
CONFIG += c++14 console


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


#DEFINES += QT_DEPRECATED_WARNINGS\
TARGET = lab7
TEMPLATE = app


SOURCES += \
    main.cpp\
    mainWindow.cpp \
    addDialog.cpp \
    editDialog.cpp \
    csv.cpp\
    csv_storage.cpp\
    generatorId.cpp\
    book.cpp


HEADERS  += mainWindow.h \
    addDialog.h \
    editDialog.h\
    storage.hpp\
    csv_storage.hpp\
    book.hpp\
    generatorId.hpp\
    csv.hpp\
    optional.hpp

FORMS    += mainWindow.ui \
    addDialog.ui \
    editDialog.ui
DISTFILES += \
    data/csv/books.csv
