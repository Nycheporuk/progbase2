#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>


#include <QListWidget>
#include <QDebug>
#include <QFileDialog>
#include <QString>
#include <QDialog>
#include <QtGui>
#include <string.h>
#include <fstream>
#include <sstream>
#include <stdio.h>

#include "addDialog.h"
#include "book.hpp"
#include "storage.hpp"
#include "generatorId.hpp"
#include "ui_mainWindow.h"
#include "csv_storage.hpp"
#include "editDialog.h"



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
signals:
    void sendBookToEdit(QListWidgetItem*);

private slots:
    void on_AddButton_clicked();

    void on_EditButton_clicked();

    void on_RemoveButton_clicked();

    void getBook(Book * book);

    void getUpdatedBook(Book * book);

    void on_actionNew_triggered();

    void on_actionOpen_triggered();

    void on_listWidget_itemClicked(QListWidgetItem *item);

private:
    Ui::MainWindow *ui;
    AddDialog * addDialog;
    EditDialog * editDialog;

    Storage * storage_ = nullptr;
    QString file_name;
};

#endif // MAINWINDOW_H
