#include "csv_storage.hpp"
#include "generatorId.hpp"

using namespace std;

Book CsvStorage::rowToBook(const CsvRow &row)
{
    Book book;
    if (row.at(0) == "0")
        book.id_ = generatorId::id_get();
    else
        book.id_ = atoi(row.at(0).c_str());
    book.name_ = row.at(1);
    book.author_ = row.at(2);
    book.chapters_ = atoi(row.at(3).c_str());

    return book;
}

CsvRow CsvStorage::BookToRow(const Book &book)
{
    CsvRow row;

    row.push_back(to_string(book.id_));
    row.push_back(book.name_);
    row.push_back(book.author_);
    row.push_back(to_string(book.chapters_));

    return row;
}

bool CsvStorage::load()
{

    string file_name = this->dir_name_;
    ifstream file;
    file.open(file_name, ios::in);
    if (file.fail())
    {
        return false;
    }
    string text_str;
    string row_str;
    while (getline(file, row_str))
    {
        text_str += row_str + '\n';
    }
    file.close();
    CsvTable table = Csv::createTableFromString(text_str);
    for (CsvRow &row : table)
    {
        Book book;
        book = rowToBook(row);
        book.id_ = generatorId::id_get();
        this->books_.push_back(book);
    }
    return true;
}
bool CsvStorage::save()
{
    //book
    string file_name = this->dir_name_;
    ofstream file;
    file.open(file_name, ios::out);
    if (file.fail())
    {
        return false;
    }
    CsvTable btable;
    for (Book &b : this->books_)
    {
        CsvRow row = BookToRow(b);
        btable.push_back(row);
    }
    
    file << Csv::createStringFromTable(btable);
    file.close();
    return true;
}

void CsvStorage::deleteAll()
{
    this->books_.clear();
}

vector<Book> CsvStorage::getAllBooks()
{
    return this->books_;
}
optional<Book> CsvStorage::getBookById(int id)
{
    for (Book &b : this->books_)
    {
        if (static_cast<int>(b.id_) == id)
        {
            return b;
        }
    }
    return nullopt;
}

bool CsvStorage::updateBook(const Book &book)
{
    auto books = getAllBooks();
    for (size_t i = 0; i < books.size(); i++)
    {
        if (books.at(i).id_ == book.id_)
        {
            this->books_.at(i).name_ = book.name_;
            this->books_.at(i).author_ = book.author_;
            this->books_.at(i).chapters_ = book.chapters_;
            return true;
        }
    }
    return false;
}
bool CsvStorage::removeBook(int id)
{
    int index = id - 1;
    auto books = getAllBooks();
    for (size_t i = 0; i < books.size(); i++)
    {
        if (books.at(i).id_ == id)
        {
            break;
        }
        if (i + 1 == books.size())
        {
            return false;
        }
    }
    this->books_.erase(this->books_.begin() + index);
    return true;
}
int CsvStorage::insertBook(const Book &book)
{
    int index = generatorId::id_get();
    Book b = book;
    b.id_ = index;
    this->books_.push_back(b);
    return index;
}
