#include "editDialog.h"
#include "ui_editDialog.h"

EditDialog::EditDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditDialog)
{
    ui->setupUi(this);
}

EditDialog::~EditDialog()
{
    delete ui;
}



void EditDialog::on_Cansel_Ok_accepted()
{
    Book * book = new Book();
    book->name_ = ui->nameLine->text().toStdString();
    book->author_ = ui->authorLine->text().toStdString();
    book->chapters_ = ui->chaptersSpinBox->value();

    emit sendUpdatedBook(book);
    EditDialog::close();
}


void EditDialog::on_Cansel_Ok_rejected()
{
    EditDialog::close();
}

void EditDialog::getBookToEdit(QListWidgetItem* item)
{
    QVariant variant = item->data(Qt::UserRole);
    Book book = variant.value<Book>();

    ui->nameLine->setText(QString::fromStdString(book.name_));
    ui->authorLine->setText(QString::fromStdString(book.author_));
    ui->chaptersSpinBox->setValue(book.chapters_);
}

