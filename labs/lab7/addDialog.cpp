#include "addDialog.h"
#include "ui_addDialog.h"

AddDialog::AddDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDialog)
{
    ui->setupUi(this);
}

AddDialog::~AddDialog()
{
    delete ui;
}

void AddDialog::on_Cansel_Ok_accepted()
{
    Book * book = new Book();
    book->name_ = ui->nameLine->text().toStdString();
    book->author_ = ui->authorLine->text().toStdString();
    book->chapters_ = ui->chaptersSpinBox->value();

    emit sendBook(book);
    AddDialog::close();

}

void AddDialog::on_Cansel_Ok_rejected()
{
    AddDialog::close();
}
