#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QDialog>
#include <QListWidgetItem>
#include "book.hpp"

namespace Ui {
class EditDialog;
}

class EditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EditDialog(QWidget *parent = 0);
    ~EditDialog();
signals:
    void sendUpdatedBook(Book*);

private slots:

    void on_Cansel_Ok_accepted();

    void on_Cansel_Ok_rejected();

    void getBookToEdit(QListWidgetItem* item);

private:
    Ui::EditDialog *ui;
};

#endif // EDITDIALOG_H
