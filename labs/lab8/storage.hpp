#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "optional.hpp"
#include "book.hpp"
#include "author.hpp"
#include "csv.hpp"

using std::string;
using std::vector;

class Storage
{

  public:
    virtual bool open() = 0;
    virtual bool close() = 0;

    // Books
    virtual vector<Book> getAllBooks(void) = 0;
    virtual optional<Book> getBookById(int id) = 0;
    virtual bool updateBook(const Book &book) = 0;
    virtual bool removeBook(int id) = 0;
    virtual int insertBook(const Book &book) = 0;
    //Authors
    virtual vector<Author> getAllAuthors(void) = 0;
    virtual optional<Author> getAuthorById(int id) = 0;
    virtual bool updateAuthor(const Author &author) = 0;
    virtual bool removeAuthor(int id) = 0;
    virtual int insertAuthor(const Author &author) = 0;
};
