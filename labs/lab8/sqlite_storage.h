#ifndef SQLITESTORAGE_H
#define SQLITESTORAGE_H

#include "storage.hpp"
#include <QtSql>
#include <QSqlDatabase>

class SqliteStorage : public Storage
{
    const string dir_name_;

    QSqlDatabase db_;



public:
    SqliteStorage(const string & dir_name);

  bool open();
  bool close();

  // Books
  vector<Book> getAllBooks(void);
  optional<Book> getBookById(int id);
  bool updateBook(const Book &book);
  bool removeBook(int id);
  int insertBook(const Book &book);
  //Authors
  vector<Author> getAllAuthors(void);
  optional<Author> getAuthorById(int id);
  bool updateAuthor(const Author &author);
  bool removeAuthor(int id);
  int insertAuthor(const Author &author);
};

#endif // SQLITESTORAGE_H
