#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "optional.hpp"
#include "book.hpp"
#include "author.hpp"
#include "csv.hpp"
#include "storage.hpp"

using std::string;
using std::vector;

class CsvStorage : public Storage
{
    const string dir_name_;

    vector<Book> books_;
    vector<Author> authors_;

    Book rowToBook(const CsvRow & row, string dir_name);
    CsvRow BookToRow(const Book & book);
    Author rowToAuthor(const CsvRow & row, string dir_name);
    CsvRow authorToRow(const Author & author);

    int getNewBookId(string dir_name);
    int getNewAuthorId(string dir_name);

    void clearIdFile();
    void deleteAll();

    int getSizeFromFile(string file_name);

public:
    CsvStorage(const string & dir_name) : dir_name_(dir_name) { }

    bool open();
    bool close();

    // books
    vector<Book> getAllBooks(void);
    optional<Book> getBookById(int id);
    bool updateBook(const Book & book);
    bool removeBook(int id);
    int insertBook(const Book & book);
    // authors
    vector<Author> getAllAuthors(void);
    optional<Author> getAuthorById(int );
    bool updateAuthor(const Author &author);
    bool removeAuthor(int id);
    int insertAuthor(const Author &author);
};
