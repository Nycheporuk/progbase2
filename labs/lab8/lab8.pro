QT += core
QT += xml
QT += sql
QT -= gui

TARGET = lab8
CONFIG += c++14 console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    cui.cpp \
    csv.cpp \
    csv_storage.cpp \
    xml_storage.cpp \
    sqlite_storage.cpp

unix|win32: LIBS += -lprogbase

HEADERS += \
    book.hpp \
    optional.hpp \
    author.hpp \
    cui.hpp \
    csv.hpp \
    csv_storage.hpp \
    storage.hpp \
    xml_storage.hpp \
    sqlite_storage.h

DISTFILES += \
    doc/КП81_ЛР6_Ничепорук_Захар.docx \
    data/id.txt \
    data/csv/books.csv \
    data/csv/authors.csv \
    data/xml/books.xml \
    data/xml/authors.xml \
`   data/sql/data.sqlite



