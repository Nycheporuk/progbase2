#pragma once
#include <string>

using namespace std;

struct Book
{
    size_t id_;
    string name_;
    string author_;
    size_t chapters_;
};
