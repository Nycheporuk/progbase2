#include "sqlite_storage.h"

#include <QDebug>
#include <QtSql>

Book getBookFromQuery(const QSqlQuery & query)
{
    Book book;
    book.id_ = query.value("id").toInt();
    book.name_ = query.value("name").toString().toStdString();
    book.author_ = query.value("author").toString().toStdString();
    book.chapters_ = query.value("chapters").toInt();
    return book;
}

Author getAuthorFromQuery(const QSqlQuery & query)
{
    Author author;
    author.id_ = query.value("id").toInt();
    author.name_ = query.value("name").toString().toStdString();
    author.country_ = query.value("country").toString().toStdString();
    author.year_ = query.value("year").toInt();
    return author;
}
SqliteStorage::SqliteStorage(const string & dir_name) : dir_name_(dir_name) {
    db_ = QSqlDatabase::addDatabase("QSQLITE");

}

bool SqliteStorage::open()
{
    QString path = QString::fromStdString(this->dir_name_) + "data.sqlite";
    db_.setDatabaseName(path);
    bool connected = db_.open();
    if(!connected)
    {
        return false;
    }

    return true;

}

bool SqliteStorage::close()
{
    db_.close();
    return true;
}

// Books
vector<Book> SqliteStorage::getAllBooks(void){

    vector<Book> books;
    QSqlQuery query("SELECT * FROM books");
    while(query.next())
    {

        books.push_back(getBookFromQuery(query));
    }
    return books;
}

optional<Book> SqliteStorage::getBookById(int id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM books WHERE id = :id");
    query.bindValue(":id", id);
    if (!query.exec()) {  // do exec if query is prepared SELECT query
       qDebug() << "get book error:" << query.lastError();       return nullopt;
       return nullopt;


    }
    if (query.next()) {
       return getBookFromQuery(query);
    } else {
       qDebug() << " not found ";
       return nullopt;

    }
}

bool SqliteStorage::updateBook(const Book &book)
{
    QSqlQuery query;
    query.prepare("UPDATE books SET name = :name, author = :author, chapters = :chapters WHERE id = :id");
    query.bindValue(":name", QString::fromStdString(book.name_));
    query.bindValue(":author", QString::fromStdString(book.author_));
    int chapters = book.chapters_;
    query.bindValue(":chapters", chapters);
    int id = book.id_;
    query.bindValue(":id", id);
    if (!query.exec()){
        qDebug() << "updateBook error:" << query.lastError();
        return false;
    }
    if((query.numRowsAffected()) == 0)
    {
        return false;
    }
    return true;
}



bool SqliteStorage::removeBook(int id)
{

    QSqlQuery query;
    query.prepare("DELETE FROM books WHERE id = :id");
    query.bindValue(":id", id);
    if (!query.exec()){
        qDebug() << "deleteBook error:" << query.lastError();
        return false;
    }
    if((query.numRowsAffected()) == 0)
    {
        return false;
    }
    return true;
}

int SqliteStorage::insertBook(const Book &book)
{


        QSqlQuery query;
        query.prepare("INSERT INTO books (name, author, chapters) VALUES (:name, :author, :chapters)");
        query.bindValue(":name", QString::fromStdString(book.name_));
        query.bindValue(":author", QString::fromStdString(book.author_));
        int chapters = book.chapters_;
        query.bindValue(":chapters", chapters);


        if(!query.exec())
       {
            qDebug() << "cant insert book" << query.lastError();
            return 0;
        }

        QVariant var = query.lastInsertId();

        return var.toInt();

}

//Authors
vector<Author> SqliteStorage::getAllAuthors(void)
{
    vector<Author> authors;
    QSqlQuery query("SELECT * FROM authors");
    while(query.next())
    {
        authors.push_back(getAuthorFromQuery(query));
    }
    return authors;
}

optional<Author> SqliteStorage::getAuthorById(int id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM authors WHERE id = :id");
    query.bindValue(":id", id);
    if (!query.exec()) {  // do exec if query is prepared SELECT query
       qDebug() << "get authors error:" << query.lastError();
       return nullopt;
    }
    if (query.next()) {
       return getAuthorFromQuery(query);
    } else {
       qDebug() << " not found ";
       return nullopt;

    }
}

bool SqliteStorage::updateAuthor(const Author &author)
{
    QSqlQuery query;
    query.prepare("UPDATE authors SET name = :name, country = :country, year = :year WHERE id = :id");
    query.bindValue(":name", QString::fromStdString(author.name_));
    query.bindValue(":country", QString::fromStdString(author.country_));
    int year = author.year_;
    query.bindValue(":year", year);
    int id = author.id_;
    query.bindValue(":id", id);
    if (!query.exec()){
        qDebug() << "updateAuthor error:" << query.lastError();
        return false;
    }
    if((query.numRowsAffected()) == 0)
    {
        return false;
    }
    return true;
}

bool SqliteStorage::removeAuthor(int id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM authors WHERE id = :id");
    query.bindValue(":id", id);
    if (!query.exec()){
        qDebug() << "deleteAuthor error:" << query.lastError();
        return false;
    }
    if((query.numRowsAffected()) == 0)
    {
        return false;
    }
    return true;
}

int SqliteStorage::insertAuthor(const Author &author)
{
    QSqlQuery query;
    query.prepare("INSERT INTO authors (name, country, year) VALUES (:name, :country, :year)");
    query.bindValue(":name", QString::fromStdString(author.name_));
    query.bindValue(":country", QString::fromStdString(author.country_));
    int year = author.year_;
    query.bindValue(":year", year);
    if(!query.exec())
    {
        qDebug() << "cant insert author" << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
    return var.toInt();
}

