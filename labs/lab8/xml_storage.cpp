#include "xml_storage.hpp"

using namespace std;

int XmlStorage::getNewBookId(string dir_name)
{
    ifstream id_stream;
    id_stream.open(dir_name + "../id.txt");
    if (id_stream.fail())
    {
        cerr << "Error getting id b" << endl;
        abort();
    }
    string book_size;
    string auth_size;
    string text;
    int i = 0;
    while (getline(id_stream, text))
    {
        if (i == 0)
            book_size += text;
        else
            auth_size += text;
        i++;
    }
    id_stream.close();

    ofstream id_upd;
    id_upd.open(dir_name + "../id.txt", ios::out);
    id_upd << atoi(book_size.c_str()) + 1 << endl;
    id_upd << atoi(auth_size.c_str());
    id_upd.close();

    return atoi(book_size.c_str()) + 1;
}
int XmlStorage::getNewAuthorId(string dir_name)
{
    ifstream id_stream;
    id_stream.open(dir_name + "../id.txt");
    if (id_stream.fail())
    {
        cerr << "Error getting id a" << endl;
        abort();
    }
    string book_size;
    string auth_size;
    string text;
    int i = 0;
    while (getline(id_stream, text))
    {
        if (i == 0)
            book_size += text;
        else
            auth_size += text;
        i++;
    }
    id_stream.close();

    ofstream id_upd;
    id_upd.open(dir_name + "../id.txt", ios::out);
    id_upd << atoi(book_size.c_str()) << endl;
    id_upd << atoi(auth_size.c_str()) + 1;
    id_upd.close();

    return atoi(auth_size.c_str()) + 1;
}

Book QDomElementToBook(QDomElement & element, string dir_name)
{
    Book book; 
    int id = XmlStorage::getNewBookId(dir_name);
    book.id_ = id;
    book.name_ = element.attribute("name").toStdString();
    book.author_ = element.attribute("author").toStdString();
    book.chapters_ = element.attribute("chapters").toInt();

    return book;

}
QDomElement BookToDomElement(QDomDocument & doc, Book & book)
{
    QDomElement el_book = doc.createElement("book");

    el_book.setAttribute("id", static_cast<int>(book.id_));
    el_book.setAttribute("name", book.name_.c_str());
    el_book.setAttribute("author", book.author_.c_str());
    el_book.setAttribute("chapters", static_cast<int>(book.chapters_));
    return el_book;
}
Author QDomElementToAuthor(QDomElement & element, string dir_name)
{
    Author author;
    int id = XmlStorage::getNewAuthorId(dir_name);
    author.id_ = id;
    author.name_ = element.attribute("name").toStdString();
    author.country_ = element.attribute("country").toStdString();
    author.year_ = element.attribute("year").toInt();

    return author;

}
QDomElement AuthorToDomElement(QDomDocument & doc, Author & author)
{
    QDomElement el_author = doc.createElement("author");

    el_author.setAttribute("id", static_cast<int>(author.id_));
    el_author.setAttribute("name", author.name_.c_str());
    el_author.setAttribute("country", author.country_.c_str());
    el_author.setAttribute("year", static_cast<int>(author.year_));
    return el_author;
}

bool XmlStorage::open()
{
    clearIdFile();
    deleteAll();
//books
    string file_name = this->dir_name_ + "books.xml";
    QString Qfile_name = QString::fromStdString(file_name);
    QFile file(Qfile_name);
    if (!file.open(QFile::ReadOnly))
    {
        qDebug() << "Error. Can not open " << Qfile_name << endl;
        return false;
    }
    QTextStream qts(&file);
    QString text = qts.readAll();
    file.close();

    QDomDocument doc;
    QString errorMsg;
    int errorLine;
    int errorCol;
    if(!doc.setContent(text, &errorMsg, &errorLine, &errorCol))
    {

        qDebug() << "line: " << errorLine << "col: " << errorCol;
        return false;
    }
    QDomElement root = doc.documentElement();
    for (int i = 0; i < root.childNodes().size(); i++)
    {
        QDomNode node = root.childNodes().at(i);
        QDomElement element = node.toElement();
        Book book = QDomElementToBook(element, this->dir_name_);
        this->books_.push_back(book);
    }
    
//authors
    file_name = this->dir_name_ + "authors.xml";
    Qfile_name = QString::fromStdString(file_name);
    QFile file2(Qfile_name);
    if (!file2.open(QFile::ReadOnly))
    {
        qDebug() << "Error. Can not open " << Qfile_name << endl;
        return false;
    }
    QTextStream qts2(&file2);
    QString text2 = qts2.readAll();
    file2.close();

 
    
    if(!doc.setContent(text2, &errorMsg, &errorLine, &errorCol))
    {
        qDebug() << "Error parsing Xml text:" << errorMsg;
        qDebug() << "line: " << errorLine << "col: " << errorCol;
        return false;
    }
    QDomElement root2 = doc.documentElement();
    for (int i = 0; i < root2.childNodes().size(); i++)
    {
        QDomNode node2 = root2.childNodes().at(i);
        QDomElement element2 = node2.toElement();
        Author auth = QDomElementToAuthor(element2, this->dir_name_);
        this->authors_.push_back(auth);
    }
    
    return true;
}
bool XmlStorage::close()
{
    //book
    QDomDocument doc;
    string file_name = this->dir_name_ + "books.xml";
    QString Qfile_name = QString::fromStdString(file_name);
    QDomElement root = doc.createElement("books");
    for (Book & book: this->books_)
    {
        QDomElement el_book = BookToDomElement(doc, book);
        root.appendChild(el_book);
    }
    doc.appendChild(root);
    QString xml_text = doc.toString(4);

    
    QFile file(Qfile_name);
    if (!file.open(QFile::WriteOnly))
    {
        qDebug() << "Error. Can not open " << Qfile_name << " for writing" << endl;
        return false;
    }

    QTextStream qts(&file);
    qts << xml_text;
    file.close();

    //author
    QDomDocument doc2;
    file_name = this->dir_name_ + "authors.xml";
    Qfile_name = QString::fromStdString(file_name);
    QDomElement root2 = doc2.createElement("authors");
    for (Author & auth: this->authors_)
    {
        QDomElement el_auth = AuthorToDomElement(doc2, auth);
        root2.appendChild(el_auth);
    }
    doc2.appendChild(root2);
    xml_text = doc2.toString(4);

    
    QFile file2(Qfile_name);
    if (!file2.open(QFile::WriteOnly))
    {
        qDebug() << "Error. Can not open " << Qfile_name << " for writing" << endl;
        return false;
    }

    QTextStream qts2(&file2);
    qts2 << xml_text;
    file2.close();

    return true;
}

void XmlStorage::clearIdFile()
{
    ofstream file;
    file.open(this->dir_name_ + "../id.txt", ios::out);
    if (file.fail())
    {
        cerr << "Error, can`t open file" << this->dir_name_ + "id.txt" << endl;
        abort();
    }
    file << 0 << "\n" << 0;
    file.close();
}
void XmlStorage::deleteAll()
{
    this->books_.clear();
    this->authors_.clear();
}

vector<Book> XmlStorage::getAllBooks()
{
    return this->books_;
}
vector<Author> XmlStorage::getAllAuthors()
{
    return this->authors_;
}
optional<Book> XmlStorage::getBookById(int id)
{
    for (Book &b : this->books_)
    {
        if (static_cast<int>(b.id_) == id)
        {
            return b;
        }
    }
    return nullopt;
}
optional<Author> XmlStorage::getAuthorById(int id)
{
    for (Author &a : this->authors_)
    {
        if (static_cast<int>(a.id_) == id)
        {
            return a;
        }
    }
    return nullopt;
}

bool XmlStorage::updateBook(const Book &book)
{
    auto books = getAllBooks();
    for (size_t i = 0; i < books.size(); i++)
    {
        if (books.at(i).id_ == book.id_)
        {
            this->books_.at(i).name_ = book.name_;
            this->books_.at(i).author_ = book.author_;
            this->books_.at(i).chapters_ = book.chapters_;
            return true;
        }
    }
    return false;
}
bool XmlStorage::updateAuthor(const Author &author)
{
    auto authors = getAllAuthors();
    for (size_t i = 0; i < authors.size(); i++)
    {
        if (authors.at(i).id_ == author.id_)
        {
            this->authors_.at(i).name_ = author.name_;
            this->authors_.at(i).country_ = author.country_;
            this->authors_.at(i).year_ = author.year_;
            return true;
        }
    }
    return false;
}
bool XmlStorage::removeBook(int id)
{
    auto books = getAllBooks();
    for (size_t i = 0; i < books.size(); i++)
    {
        if (books.at(i).id_ == static_cast<size_t>(id))
        {
            break;
        }
        if (i + 1 == books.size())
        {
            return false;
        }
    }
    this->books_.erase(this->books_.begin() + id - 1);
    return true;
}
bool XmlStorage::removeAuthor(int id)
{
    auto authors = getAllAuthors();
    for (size_t i = 0; i < authors.size(); i++)
    {
        if (authors.at(i).id_ == static_cast<size_t>(id))
        {
            break;
        }
        if (i + 1 == authors.size())
        {
            return false;
        }
    }
    this->authors_.erase(this->authors_.begin() + id - 1);
    return true;
}
int XmlStorage::insertBook(const Book &book)
{
    int index = getNewBookId(this->dir_name_);
    Book b = book;
    b.id_ = index;
    this->books_.push_back(b);
    return index;
}
int XmlStorage::insertAuthor(const Author &author)
{
    int index = getNewAuthorId(this->dir_name_);
    Author a = author;
    a.id_ = index;
    this->authors_.push_back(a);
    return index;
}
