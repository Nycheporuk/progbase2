#pragma once
#include "storage.hpp"
#include <progbase.h>
#include <progbase/console.h>

class Cui
{
    Storage * const storage_;

    void mainMenu();
    void Menu(int id);
    void UpdateMenu(int id);
    void DeleteMenu(int id);
    void InsertMenu(int id);
    void SaveMenu(int id);

    void printBook   (vector<Book> & book);
    void printAuthor   (vector<Author> & auth);

public:
    Cui(Storage * storage): storage_(storage) {}
    
    void show();
};
