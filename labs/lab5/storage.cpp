#include "storage.hpp"

using namespace std;
static Book rowToBook(const CsvRow &row)
{
    Book book;

    book.id_ = atoi(row.at(0).c_str());
    book.name_ = row.at(1);
    book.author_ = row.at(2);
    book.chapters_ = atoi(row.at(3).c_str());

    return book;
}

static CsvRow BookToRow(const Book &book)
{
    CsvRow row;

    row.push_back(to_string(book.id_));
    row.push_back(book.name_);
    row.push_back(book.author_);
    row.push_back(to_string(book.chapters_));

    return row;
}

static Author rowToAuthor(const CsvRow &row)
{
    Author auth;

    auth.id_ = atoi(row.at(0).c_str());
    auth.name_ = row.at(1);
    auth.country_ = row.at(2);
    auth.year_ = atoi(row.at(3).c_str());

    return auth;
}

static CsvRow AuthorToRow(const Author &auth)
{
    CsvRow row;

    row.push_back(to_string(auth.id_));
    row.push_back(auth.name_);
    row.push_back(auth.country_);
    row.push_back(to_string(auth.year_));

    return row;
}

int Storage::getNewBookId()
{
    ifstream id_stream;
    id_stream.open(this->dir_name_ + "id.txt");
    if (id_stream.fail())
    {
        cerr << "Error getting id" << endl;
        abort();
    }
    string book_size;
    string auth_size;
    string text;
    int iter = 0;
    while (getline(id_stream, text))
    {
        if (iter == 0)
            book_size += text;
        else
            auth_size += text;
        iter++;
    }
    id_stream.close();

    ofstream id_upd;
    id_upd.open(this->dir_name_ + "id.txt", ios::out);
    id_upd << atoi(book_size.c_str()) + 1 << endl;
    id_upd << atoi(auth_size.c_str());
    id_upd.close();

    return atoi(book_size.c_str()) + 1;
}
int Storage::getNewAuthorId()
{
    ifstream id_stream;
    id_stream.open(this->dir_name_ + "id.txt");
    if (id_stream.fail())
    {
        cerr << "Error getting id" << endl;
        abort();
    }
    string book_size;
    string auth_size;
    string text;
    int iter = 0;
    while (getline(id_stream, text))
    {
        if (iter == 0)
            book_size += text;
        else
            auth_size += text;
        iter++;
    }
    id_stream.close();

    ofstream id_upd;
    id_upd.open(this->dir_name_ + "id.txt", ios::out);
    id_upd << atoi(book_size.c_str()) << endl;
    id_upd << atoi(auth_size.c_str()) + 1;
    id_upd.close();

    return atoi(auth_size.c_str()) + 1;
}
bool Storage::load()
{
    clearIdFile();
    deleteAll();
    string file_name = this->dir_name_ + "books.csv";
    ifstream file;
    file.open(file_name, ios::in);
    if (file.fail())
    {
        return false;
    }
    string text_str;
    string row_str;
    while (getline(file, row_str))
    {
        text_str += row_str + '\n';
    }
    file.close();
    CsvTable table = Csv::createTableFromString(text_str);
    for (CsvRow &row : table)
    {
        Book book;
        book.id_ = getNewBookId();
        book.name_ = row.at(1);
        book.author_ = row.at(2);
        book.chapters_ = atoi(row.at(3).c_str());
        this->books_.push_back(book);
    }
//authors
    file_name = this->dir_name_ + "authors.csv";
    ifstream file2;
    file2.open(file_name, ios::in);
    if (file2.fail())
    {
        return false;
    }
    string text_str2;
    string row_str2;
    while (getline(file2, row_str2))
    {
        text_str2 += row_str2 + '\n';
    }
    file2.close();
    CsvTable table2 = Csv::createTableFromString(text_str2);
    for (CsvRow &row2 : table2)
    {
        Author auth;
        auth.id_ = getNewAuthorId();
        auth.name_ = row2.at(1);
        auth.country_ = row2.at(2);
        auth.year_ = atoi(row2.at(3).c_str());
        this->authors_.push_back(auth);
    }
    return true;
}
bool Storage::save()
{
    //book
    string file_name = this->dir_name_ + "books.csv";
    ofstream file;
    file.open(file_name, ios::out);
    if (file.fail())
    {
        return false;
    }
    int i = 0;
    for (Book &b : this->books_)
    {
        Book book = this->books_.at(i);
        file << book.id_ << ",";
        file << book.name_ << ",";
        file << book.author_ << ",";
        file << book.chapters_ << ",";
        if (i < static_cast<int>(this->books_.size() - 1))
        {
            file << "\n";
        }
        i++;
    }
    file.close();
    //author
    file_name = this->dir_name_ + "authors.csv";
    ofstream file2;
    file.open(file_name, ios::out);
    if (file2.fail())
    {
        return false;
    }
    int j = 0;
    for (Author &a : this->authors_)
    {
        Author auth = this->authors_.at(j);
        file2 << auth.id_ << ",";
        file2 << auth.name_ << ",";
        file2 << auth.country_ << ",";
        file2 << auth.year_ << ",";
        if (i < static_cast<int>(this->authors_.size() - 1))
        {
            file2 << "\n";
        }
        j++;
    }
    file2.close();
    return true;
}

void Storage::clearIdFile()
{
    ofstream file;
    file.open(this->dir_name_ + "id.txt", ios::out);
    if (file.fail())
    {
        cout << "Error, can`t open file" << endl;
        abort();
    }
    file << 0 << "\n"
         << 0;
    file.close();
}
void Storage::deleteAll()
{
    this->books_.clear();
    this->authors_.clear();
}
vector<Book> Storage::getAllBooks()
{
    return this->books_;
}
vector<Author> Storage::getAllAuthors()
{
    return this->authors_;
}
optional<Book> Storage::getBookById(int id)
{
    for (Book &b : this->books_)
    {
        if (static_cast<int>(b.id_) == id)
        {
            return b;
        }
    }
    return nullopt;
}
optional<Author> Storage::getAuthorById(int id)
{
    for (Author &a : this->authors_)
    {
        if (static_cast<int>(a.id_) == id)
        {
            return a;
        }
    }
    return nullopt;
}

bool Storage::updateBook(const Book &book)
{
    auto books = getAllBooks();
    for (size_t i = 0; i < books.size(); i++)
    {
        if (books.at(i).id_ == book.id_)
        {
            this->books_.at(i).name_ = book.name_;
            this->books_.at(i).author_ = book.author_;
            this->books_.at(i).chapters_ = book.chapters_;
            return true;
        }
    }
    return false;
}
bool Storage::updateAuthor(const Author &author)
{
    auto authors = getAllAuthors();
    for (size_t i = 0; i < authors.size(); i++)
    {
        if (authors.at(i).id_ == author.id_)
        {
            this->authors_.at(i).name_ = author.name_;
            this->authors_.at(i).country_ = author.country_;
            this->authors_.at(i).year_ = author.year_;
            return true;
        }
    }
    return false;
}
bool Storage::removeBook(int id)
{
    int index = id - 1;
    auto books = getAllBooks();
    for (size_t i = 0; i < books.size(); i++)
    {
        if (books.at(i).id_ == static_cast<size_t>(id))
        {
            break;
        }
        if (i + 1 == books.size())
        {
            return false;
        }
    }
    this->books_.erase(this->books_.begin() + index);
    return true;
}
bool Storage::removeAuthor(int id)
{
    int index = id - 1;
    auto authors = getAllAuthors();
    for (size_t i = 0; i < authors.size(); i++)
    {
        if (authors.at(i).id_ == static_cast<size_t>(id))
        {
            break;
        }
        if (i + 1 == authors.size())
        {
            return false;
        }
    }
    this->authors_.erase(this->authors_.begin() + index);
    return true;
}
int Storage::insertBook(const Book &book)
{
    int index = getNewBookId();
    Book b = book;
    b.id_ = index;
    this->books_.push_back(b);
    return index;
}
int Storage::insertAuthor(const Author &author)
{
    int index = getNewAuthorId();
    Author a = author;
    a.id_ = index;
    this->authors_.push_back(a);
    return index;
}
