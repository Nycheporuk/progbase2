#include <fstream>
#include <iostream>
#include <istream>

#include "csv.hpp"
#include "storage.hpp"
#include "cui.hpp"

using namespace std;
using namespace Csv;

int main()
{
    Storage storage("../data/");
    if (!storage.load())
    {
        cerr << "Can't load storage" << endl;
        abort();
    }

    Cui cui(&storage);    
    cui.show();
    return 0;
}