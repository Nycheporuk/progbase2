#include "cui.hpp"

void Cui::mainMenu()
{
    int input;
    while (true)
    {
        Console_clear();
        cout << "Enter entity" << endl;
        cout << "1. Book" << endl;
        cout << "2. Author" << endl;
        cout << "3. Clear file with IDs" << endl;
        cout << "4. Exit" << endl;

        std::cin >> input;

        switch (input)
        {
        case 1: 
            Menu(input);
            break;
        
        case 2: 
            Menu(input);
            break;
        
        case 3: 
            storage_->load();
            break;
        case 4: 
            Console_clear();
            exit(EXIT_SUCCESS);
            break;
        default: 
            break;
        }
    }
}

void Cui::Menu(int entity_id)
{
    int input;

    while (true)
    {
        Console_clear();

        if (entity_id == 1)
        {
            cout << "Enter option" << endl;
            cout << "1. Insert book" << endl;
            cout << "2. Remove book" << endl;
            cout << "3. Update book" << endl;
            cout << "4. Save changes" << endl;
            cout << "5. Back" << endl;

            vector<Book> book = storage_->getAllBooks();
            printBook(book);
        }
        else 
        {
            cout << "Enter option" << endl;
            cout << "1. Insert Author" << endl;
            cout << "2. Remove Author" << endl;
            cout << "3. Update Author" << endl;
            cout << "4. Save changes" << endl;
            cout << "5. Back" << endl;

            vector<Author> Author = storage_->getAllAuthors();
            printAuthor(Author);
        }
            
        std::cin >> input;

        switch (input)
        {
        case 1: 
            InsertMenu(entity_id);
            break;
        case 2:
            DeleteMenu(entity_id);
            break;
        case 3:
            UpdateMenu(entity_id);
            break;
        case 4:
            SaveMenu(entity_id);
            break;
        case 5: 
            mainMenu();
        default:
            break;
        }
        
        
    }
}

void Cui::UpdateMenu(int entity_id)
{
    while (true)
    {
        Console_clear();
        cout << "Update menu" << endl;

        if (entity_id == 1)
        {
            size_t index_;
            cout << "Enter index: ";
            cin >> index_;

            string name_;
            cout << "Enter name: ";
            cin >> name_;

            string author_;
            cout << "Enter author: ";
            cin >> author_;

            size_t chapters_;
            cout << "Enter chapters: ";
            cin >> chapters_;

            Book book;
            book.id_ = index_;
            book.name_ = name_;
            book.author_ = author_;
            book.chapters_ = chapters_;

            storage_->updateBook(book);
        }
        else 
        {
            size_t index_;
            cout << "Enter index: ";
            cin >> index_;

            string name_;
            cout << "Enter name: ";
            cin >> name_;

            string country_;
            cout << "Enter country: ";
            cin >> country_;

            size_t year_;
            cout << "Enter year: ";
            cin >> year_;

            Author author;
            author.id_ = index_;
            author.name_ = name_;
            author.country_ = country_;
            author.year_ = year_;

            storage_->updateAuthor(author);
        }
        Menu(entity_id);
    }
}

void Cui::DeleteMenu(int entity_id)
{
    while (true)
    {
        Console_clear();
        cout << "Delete menu" << endl;

        if (entity_id == 1)
        {
            cout << "Enter book id to delete" << endl;

            string id;
            cin >> id;

            if(!storage_->removeBook(atoi(id.c_str())))
            {
                cout << "id doesn't exist" << endl;
                Console_getChar();
            }
        }
        else 
        {
            cout << "Enter Author id to delete" << endl;

            string id;
            cin >> id;

            if(!storage_->removeAuthor(atoi(id.c_str())))
            {
                cout << "id doesn't exist." << endl;
                Console_getChar();
            }
        }
        Menu(entity_id);
    }
}

void Cui::InsertMenu(int entity_id)
{
    while (true)
    {
        Console_clear();
        cout << "Insert menu" << endl;

        if (entity_id == 1)
        {
            string name_;
            cout << "Enter name: ";
            cin >> name_;

            string author_;
            cout << "Enter author: ";
            cin >> author_;

            size_t chapters_;
            cout << "Enter chapters: ";
            cin >> chapters_;

            Book book;
            book.name_ = name_;
            book.author_ = author_;
            book.chapters_ = chapters_;

            storage_->insertBook(book);
        }
        else 
        {
            string name_;
            cout << "Enter name: ";
            cin >> name_;

            string country_;
            cout << "Enter country: ";
            cin >> country_;

            size_t year_;
            cout << "Enter year: ";
            cin >> year_;

            Author author;
            author.name_ = name_;
            author.country_ = country_;
            author.year_ = year_;

            storage_->insertAuthor(author);
        }
        Menu(entity_id);   
    }
}

void Cui::show()
{
    Console_clear();
    mainMenu();
    Console_clear();
}

void Cui::SaveMenu(int entity_id)
{
    if(!storage_->save())
    {
        cerr << "Can not open output file" << endl;
        abort();
    }

    cout << "saved" << endl;
    Console_getChar();

    Menu(entity_id);
}

void Cui::printBook  (vector<Book> & book)
{
    printf("\n");
    for (Book & b : book)
    {
        cout << b.id_ << "," << b.name_ << "," << b.author_ << "," << b.chapters_ << endl;  
    }
}

void Cui::printAuthor   (vector<Author> & auth)
{
    printf("\n");
    for (Author & a : auth)
    {
        cout << a.id_ << "," << a.name_ << "," << a.country_ << "," << a.year_ << endl;  
    }
}
