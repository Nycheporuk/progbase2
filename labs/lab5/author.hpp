#include <string>

using namespace std;

struct Author
{
    size_t id_;
    string name_;
    string country_;
    size_t year_;
};