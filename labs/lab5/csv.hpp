#pragma once
#include <vector>
#include <string>
#include <cstdlib>
#include <cctype>
#include <cstdio>
#include <cstdbool>


using std::string;
using std::vector;

using CsvRow = vector<string>;  
using CsvTable = vector<CsvRow>; 

namespace Csv
{
CsvTable createTableFromString(const string &csvStr); 
string createStringFromTable(const CsvTable &csvTable); 
} 