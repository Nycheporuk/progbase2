#include "csv.hpp"

CsvTable Csv::createTableFromString(const string & csvStr)
{
    CsvTable table;
    CsvRow row;
    const char * csvString = csvStr.c_str();
    char buf[100];
    int bufX = 0;

    while (true)
    {
        if (*csvString == ',')
        {
            buf[bufX] = '\0';
            bufX = 0;
            row.push_back(buf);
        }
        else if (*csvString == '\n')
        {
            if (bufX != 0)
            {
                buf[bufX] = '\0';
                bufX = 0;
                row.push_back(buf);
                table.push_back(row);
            }
            row.clear();
        }
        else if (*csvString == '\0')
        {
            if (bufX == 0)
            {
                break;
            }

            buf[bufX] = '\0';
            row.push_back(buf);
            table.push_back(row);
            break;
        }
        else
        {
            buf[bufX++] = *csvString;
        }

        csvString += 1;
    }
    return table;
}

string Csv::createStringFromTable(const CsvTable &csvTable)
{
    string csvData;

    for (size_t i = 0; i < csvTable.size(); i++)
    {
        CsvRow row = csvTable.at(i);
        for (size_t j = 0; j < row.size(); j++)
        {
            csvData += row.at(j);

            if (j != row.size() - 1)
            {
                csvData += ',';
            }
        }
        if (i != csvTable.size())
        {
            csvData += '\n';
        }
    }
    return csvData;
}