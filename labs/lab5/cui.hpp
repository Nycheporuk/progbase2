#include "storage.hpp"
#include <progbase.h>
#include <progbase/console.h>
#include <iostream>

class Cui
{
    Storage * const storage_;

    void mainMenu();
    void Menu(int entity_id);
    void UpdateMenu(int entity_id);
    void DeleteMenu(int entity_id);
    void InsertMenu(int entity_id);
    void SaveMenu(int entity_id);

    void printBook   (vector<Book> & book);
    void printAuthor   (vector<Author> & auth);

public:
    Cui(Storage * storage): storage_(storage) {}
    void show();
};