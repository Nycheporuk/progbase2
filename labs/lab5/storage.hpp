#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "optional.hpp"
#include "book.hpp"
#include "author.hpp"
#include "csv.hpp"

using std::string;
using std::vector;

class Storage
{
    const string dir_name_;

    vector<Book> books_;
    vector<Author> authors_;

    static Book rowToBook(const CsvRow &row);
    static CsvRow BookToRow(const Book &st);
    static Author rowToAuthor(const CsvRow &row);
    static CsvRow AuthorToRow(const Author &st);

    int getNewBookId();
    int getNewAuthorId();


    void clearIdFile();
    void deleteAll();
  public:
    Storage(const string &dir_name) : dir_name_(dir_name) {}

    bool load();
    bool save();
    // Books
    vector<Book> getAllBooks(void);
    optional<Book> getBookById(int id);
    bool updateBook(const Book &book);
    bool removeBook(int id);
    int insertBook(const Book &book);
    //Authors
    vector<Author> getAllAuthors(void);
    optional<Author> getAuthorById(int id);
    bool updateAuthor(const Author &author);
    bool removeAuthor(int id);
    int insertAuthor(const Author &author);
};
