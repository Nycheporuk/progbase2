#pragma once

#include <stdlib.h> 
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

typedef struct KeyValue KeyValue;
struct KeyValue
{
    const char * key;
    const char * value;
};

typedef struct __SortedKeyValueList SortedKeyValueList;
struct __SortedKeyValueList {
   KeyValue  * items;
   size_t capacity;
   size_t size;
};

int      KeyValue_compare            (KeyValue * a, KeyValue * b);

void     SortedKeyValueList_init     (SortedKeyValueList * self);
void     SortedKeyValueList_deinit   (SortedKeyValueList * self);

size_t   SortedKeyValueList_size     (SortedKeyValueList * self);
KeyValue SortedKeyValueList_get      (SortedKeyValueList * self, int index);
void     SortedKeyValueList_set      (SortedKeyValueList * self, int index, KeyValue value);
void     SortedKeyValueList_removeAt (SortedKeyValueList * self, int index);

void     SortedKeyValueList_add      (SortedKeyValueList * self, KeyValue value);  
void     SortedKeyValueList_remove   (SortedKeyValueList * self, KeyValue value);
int      SortedKeyValueList_indexOf  (SortedKeyValueList * self, KeyValue value);  
bool     SortedKeyValueList_contains (SortedKeyValueList * self, KeyValue value);  
 
void     SortedKeyValueList_clear    (SortedKeyValueList * self);
void     SortedKeyValueList_print    (SortedKeyValueList * self);
void     SortedKeyValueList_realloc  (SortedKeyValueList * self);