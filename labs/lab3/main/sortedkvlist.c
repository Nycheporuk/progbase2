#include "sortedkvlist.h"

int KeyValue_compare(KeyValue *a, KeyValue *b)
{
    int i = strcmp(a->key, b->key);
    return i;
}

void SortedKeyValueList_init(SortedKeyValueList *self)
{
    self->capacity = 4;
    self->size = 0;
    self->items = malloc(sizeof(KeyValue) * self->capacity);
}
void SortedKeyValueList_deinit(SortedKeyValueList *self)
{
    free(self->items);
}

size_t SortedKeyValueList_size(SortedKeyValueList *self)
{
    return self->size;
}
KeyValue SortedKeyValueList_get(SortedKeyValueList *self, int index)
{
    return self->items[index];
}
void SortedKeyValueList_set(SortedKeyValueList *self, int index, KeyValue value)
{
    if (SortedKeyValueList_indexOf(self, value) == -1)
    {
        return;
    }
    KeyValue item = SortedKeyValueList_get(self, index);
    if (KeyValue_compare(&item, &value) == 0)
    {
        self->items[index] = value;
    }
}
void SortedKeyValueList_removeAt(SortedKeyValueList *self, int index)
{
    for (int i = index; i < self->size - 1; i++)
    {
        self->items[i] = self->items[i + 1];
    }
    self->size--;
}

void SortedKeyValueList_add(SortedKeyValueList *self, KeyValue value)
{
    if (self->size == self->capacity)
    {
        SortedKeyValueList_realloc(self);
    }

    self->items[self->size++] = value;

} // add value and sort, tip: use insertion sort
void SortedKeyValueList_remove(SortedKeyValueList *self, KeyValue value)
{
    int index = SortedKeyValueList_indexOf(self, value);
    if (index == -1)
    {
        return;
    }
    SortedKeyValueList_removeAt(self, index);
} 
int SortedKeyValueList_indexOf(SortedKeyValueList *self, KeyValue value)
{
    for (int i = 0; i < SortedKeyValueList_size(self); i++)
    {
        if (KeyValue_compare(&value, &self->items[i]) == 0)
        {
            return i;
        }
    }
    return -1;
} // < O(n), tip: use non-linear search method
bool SortedKeyValueList_contains(SortedKeyValueList *self, KeyValue value)
{
    return SortedKeyValueList_indexOf(self, value) != -1;
} // < O(n), tip: find index and check it for -1

void SortedKeyValueList_clear(SortedKeyValueList *self)
{
    self->size = 0;
}

void SortedKeyValueList_print(SortedKeyValueList *self)
{
    for (int i = 0; i < self->size; i++)
    {
        printf("Key: %s; Value: %s\n", self->items[i].key, self->items[i].value);
    }
}

void SortedKeyValueList_realloc(SortedKeyValueList *self)
{
    self->capacity *= 2;
    KeyValue *new_item = realloc(self->items, sizeof(KeyValue) * self->capacity);
    if (new_item == NULL)
    {
        fprintf(stderr, "Can not realloc\n");
        exit(EXIT_FAILURE);
    }
    self->items = new_item;
}
