#pragma once

#include <stdlib.h> 
#include <stdbool.h> 
#include <assert.h>
#include <string.h>

#include "sortedkvlist.h"

typedef struct __StrStrMap StrStrMap;
struct __StrStrMap
{
    SortedKeyValueList list;
};

StrStrMap  * StrStrMap_alloc        ();

void         StrStrMap_init         (StrStrMap * self);
void         StrStrMap_deinit       (StrStrMap * self);
void         StrStrMap_free         (StrStrMap * self);

size_t       StrStrMap_size         (StrStrMap * self);

void         StrStrMap_add          (StrStrMap * self, const char * key, const char * value);
bool         StrStrMap_contains     (StrStrMap * self, const char * key);
const char * StrStrMap_get          (StrStrMap * self, const char * key);
const char * StrStrMap_set          (StrStrMap * self, const char * key, const char * value);
const char * StrStrMap_remove       (StrStrMap * self, const char * key);
void         StrStrMap_clear        (StrStrMap * self);

StrStrMap  * createMuseumMap        (int id, const char * name, const char * author, size_t chapters, const char * status);
char       * String_allocCopy       (const char * value);
char       * String_allocFromSize_t (size_t value);
char       * String_allocFromDouble (double value);
char       * String_allocFromInt    (int value);

void         StrStrMap_printString  (StrStrMap * self, int ind);
void         StrStrMap_print        (StrStrMap * self);

void         StrStrMap_strToMap     (StrStrMap * self, char * str);

void         StrStrMap_mapToStr     (StrStrMap * self, char * str);

void         StrStrMap_values       (StrStrMap * self, SortedKeyValueList * values); 