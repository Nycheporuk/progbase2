#include "strstrmap.h"

StrStrMap *StrStrMap_alloc()
{
    StrStrMap *map = malloc(sizeof(StrStrMap));
    StrStrMap_init(map);
    return map;
}

void StrStrMap_init(StrStrMap *self)
{
    SortedKeyValueList_init(&self->list);
}
void StrStrMap_deinit(StrStrMap *self)
{
    for (int i = 0; i < self->list.size; i++)
    {
        void *p = (void *)self->list.items[i].value;
        free(p);
    }
    SortedKeyValueList_deinit(&self->list);
}

void StrStrMap_free(StrStrMap *self)
{
    StrStrMap_deinit(self);
    free(self);
}

size_t StrStrMap_size(StrStrMap *self)
{
    return SortedKeyValueList_size(&self->list);
}

void StrStrMap_add(StrStrMap *self, const char *key, const char *value)
{
    KeyValue kv;
    kv.key = key;
    kv.value = value;
    SortedKeyValueList_add(&self->list, kv);
}

bool StrStrMap_contains(StrStrMap *self, const char *key)
{
    KeyValue kv;
    kv.key = key;
    return SortedKeyValueList_contains(&self->list, kv);
}
const char *StrStrMap_get(StrStrMap *self, const char *key)
{
    KeyValue kv;
    kv.key = key;
    int index = SortedKeyValueList_indexOf(&self->list, kv);
    if (index == -1)
    {
        fprintf(stderr, "Error\n");
    }
    KeyValue val = SortedKeyValueList_get(&self->list, index);
    return val.value;
}
const char *StrStrMap_set(StrStrMap *self, const char *key, const char *value)
{
    KeyValue kv;
    kv.key = key;
    kv.value = value;
    int index = SortedKeyValueList_indexOf(&self->list, kv);
    if (index == -1)
    {
        fprintf(stderr, "Error\n");
    }
    KeyValue old = SortedKeyValueList_get(&self->list, index);
    SortedKeyValueList_set(&self->list, index, kv);
    return old.value;
}
const char *StrStrMap_remove(StrStrMap *self, const char *key)
{
    KeyValue kv;
    kv.key = key;
    int index = SortedKeyValueList_indexOf(&self->list, kv);
    if (index == -1)
    {
        fprintf(stderr, "Error\n");
    }
    KeyValue old = SortedKeyValueList_get(&self->list, index);
    SortedKeyValueList_remove(&self->list, kv);
    return old.value;
}
void StrStrMap_clear(StrStrMap *self)
{
    SortedKeyValueList_clear(&self->list);
}

StrStrMap *createBookMap(int id, const char *name, const char *author, size_t chapters, const char *status)
{
    char *idValue = String_allocFromInt(id);
    char *nameValue = String_allocCopy(name);
    char *authorValue = String_allocCopy(author);
    char *chaptValue = String_allocFromSize_t(chapters);
    char *statusValue = String_allocCopy(status);

    StrStrMap *map = StrStrMap_alloc();
    StrStrMap_add(map, "id", idValue);
    StrStrMap_add(map, "name", nameValue);
    StrStrMap_add(map, "author", authorValue);
    StrStrMap_add(map, "chapters", chaptValue);
    StrStrMap_add(map, "status", statusValue);

    return map;
}
char *String_allocCopy(const char *value)
{
    char *strOnHeap = malloc(sizeof(char) * (strlen(value)) + 1);
    strcpy(strOnHeap, value);
    return strOnHeap;
}
char *String_allocFromSize_t(size_t value)
{
    char size_tToString[10];
    sprintf(size_tToString, "%li", value);
    char *strOnHeap = malloc(sizeof(char) * strlen(size_tToString) + 1);
    strcpy(strOnHeap, size_tToString);
    return strOnHeap;
}
char *String_allocFromInt(int value)
{
    char intToString[10];
    sprintf(intToString, "%i", value);
    char *strOnHeap = malloc(sizeof(char) * strlen(intToString) + 1);
    strcpy(strOnHeap, intToString);
    return strOnHeap;
}
char *String_allocFromDouble(double value)
{
    char doubleToString[12];
    sprintf(doubleToString, "%f", value);
    char *strOnHeap = malloc(sizeof(char) * strlen(doubleToString) + 1);
    strcpy(strOnHeap, doubleToString);
    return strOnHeap;
}

void StrStrMap_printString(StrStrMap *self, int ind)
{
    printf("%s: ", self->list.items[ind].key);
    printf("%s; ", self->list.items[ind].value);
    printf("%s: ", self->list.items[ind + 1].key);
    printf("%s; ", self->list.items[ind + 1].value);
    printf("%s: ", self->list.items[ind + 2].key);
    printf("%s; ", self->list.items[ind + 2].value);
    printf("%s: ", self->list.items[ind + 3].key);
    printf("%s; ", self->list.items[ind + 3].value);
    printf("%s: ", self->list.items[ind + 4].key);
    printf("%s.\n", self->list.items[ind + 4].value);
}

void StrStrMap_print(StrStrMap *self)
{
    if (self == NULL || self->list.size == 0)
    {
        printf("Map does not exist or is empty\n");
        return;
    }
    for (int i = 0; i < self->list.size; i += 5)
    {
        StrStrMap_printString(self, i);
    }
}

void StrStrMap_keys(StrStrMap *self, SortedKeyValueList *keys)
{
    for (int i = 0; i < StrStrMap_size(self); i++)
    {
        SortedKeyValueList list = self->list;
        SortedKeyValueList_print(&list);
    }
}

void StrStrMap_values(StrStrMap *self, SortedKeyValueList *values)
{
}

static void insertCharIntoArray(char *arr, char c, int ind)
{
    int len = strlen(arr);
    for (int i = len; i > ind; i--)
    {
        arr[i] = arr[i - 1];
    }
    arr[ind] = c;
}

static void prArray(char *buf)
{
    int bufX = 0;
    bool quot = true;

    while (buf[bufX])
    {
        if (quot)
        {
            insertCharIntoArray(buf, '"', 0);
            strcat(buf, "\"");
            bufX += 2;
            quot = false;
        }

        if (buf[bufX] == '"' && buf[bufX + 1] != '\0')
        {
            insertCharIntoArray(buf, '"', bufX);
            bufX += 1;
        }

        bufX += 1;
    }
}

static void clearArr(char *arr)
{
    for (int i = 0; i < 100; i++)
    {
        arr[i] = '\0';
    }
}

void StrStrMap_mapToStr(StrStrMap *self, char *str)
{
    char buf[100];
    int bufX = 0;
    buf[bufX] = '\0';

    if (strlen(str) > 0)
        strcat(str, "\n");
    strcat(str, self->list.items[0].value);
    strcat(str, ",");

    strcat(buf, self->list.items[1].value);
    prArray(buf);
    strcat(str, buf);
    buf[0] = '\0';
    strcat(str, ",");
    clearArr(buf);

    strcat(buf, self->list.items[2].value);
    prArray(buf);
    strcat(str, buf);
    buf[0] = '\0';
    strcat(str, ",");
    clearArr(buf);

    strcat(str, self->list.items[3].value);
    strcat(str, ",");

    strcat(buf, self->list.items[4].value);
    prArray(buf);
    strcat(str, buf);
}
