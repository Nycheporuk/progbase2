#pragma once

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <unistd.h>  
#include <getopt.h>

typedef struct List List;
struct List
{
    int capacity;
    int size;
    void **items;
};

struct Ranobe
{
    char *name;
    char *author;
    int  numOfChapters;
    char *status;
};

List * List_alloc   ();         
void   List_free    (List *self); 

void   List_init    (List *self);   
void   List_deinit  (List *self); 

void * List_get     (List *self, int index); 
void   List_set     (List *self, int index, void *value); 
size_t List_size    (List *self); 
void   List_insert  (List *self, int index, void *value); 
void   List_removeAt(List *self, int index); 

void   List_add     (List *self, void *value); 
void   List_remove  (List *self, void *value); 
int    List_indexOf (List *self, void *value); 
bool   List_contains(List *self, void *value); 
bool   List_isEmpty (List *self); 

void * strOnHeap(void *str); 
void   List_printCSV(List *self);

void   List_realloc(List *self);

List * createListFromTable(List * csvTable);

void   processItems(List * items, int n);

List * List_toTable(List * self); 
void   List_valueFromListToStr(List * self, char * str); 

void List_print(List * self); 
void List_clearStruct(List * self); 