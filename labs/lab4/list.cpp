#include "list.hpp"

List::List()
{
    this->capacity_ = 12;
    this->items_ = new char *[this->capacity_];
    this->size_ = 0;
}
List::~List()
{
    delete[] this->items_;
}
char *List::get(int index)
{
    return this->items_[index];
}
void List::set(int index, char *value)
{
    this->items_[index] = value;
}
int List::size()
{
    return this->size_;
}
void List::print()
{
    for (int i = 0; i < this->size_; i++)
    {
        std::cout << List::get(i) << std::endl;
    }
}
void List::add(char *str)
{
    this->items_[this->size_] = str;
    this->size_ += 1;
}
void List::reorder(){
    std::cout << "\n<Reorder list>\n" << std::endl;
    for(int i = 0; i < this->size_; i++) 
    {
        if(hasNoDigit(List::get(i))) 
        {
            if(i == 0) 
            {
                continue;
            }  
            char * tmp = this->items_[i];

            for(int j = i; j > 0; j--) 
            {
                this->items_[j] = this->items_[j - 1];
            }
            this->items_[0] = tmp;
        }
    }
}

bool List::hasNoDigit(char *str)
{
    for (int i = 0; i < static_cast<int>(strlen(str)); i++)
    {
        if (isdigit(str[i]))
        {
            return false;
        }
    }
    return true;
}
