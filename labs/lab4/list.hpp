#pragma once
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cstring>
#include <iostream>

class List
{
    char **items_;
    int capacity_;
    int size_;

bool hasNoDigit(char *str);
  public:
    List();
    ~List();

    char *get(int index);
    void set(int index, char *value);
    void add(char *str);

    void print();

    int size();

    bool isEmpty();
    void reorder();
    
};