#pragma once
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdbool>
#include <iostream>

class Queue
{
  char **items_;
  int capacity_;
  int size_;

  void moveList();

public:
  Queue();
  ~Queue();
  int size();
  void enqueue(char *value);
  char *dequeue();
  bool isEmpty();
  void print();
};