
#include "list.hpp"
#include "queue.hpp"

int main()
{
List list = List();

FILE *fp = fopen("data.txt", "r");
if (fp == nullptr)
exit(EXIT_FAILURE);

fseek(fp, 0, SEEK_END);
const int n = ftell(fp);
fseek(fp, 0, SEEK_SET);

char *ch = new char[n + 1];
int i = 0;
int j = 0;
ch[i++] = fgetc(fp);

while (ch[i - 1] != EOF)
{
if (ch[i - 1] == '\n')
{
ch[i - 1] = '\0';

list.add(&ch[j]);

j = i;
}
if (n == i)
{
list.add(&ch[j]);
}
ch[i++] = fgetc(fp);
}

ch[i - 1] = '\0';
fclose(fp);

std::cout << "\nList" << std::endl;
list.print();
list.reorder();
list.print();
Queue queue1;
Queue queue2;
std::cout << "\n<Fill queue from list>\n" << std::endl;

for (int i = 0; i < list.size(); i++)
{
if (i % 2 == 1)
queue2.enqueue(list.get(i));

else
queue1.enqueue(list.get(i));
}
std::cout << "Queue1" << std::endl;
queue1.print();
std::cout << "Queue2" << std::endl;
queue2.print();
List list2;
std::cout << "<Fill list from queues>" << std::endl;
int q1 = queue1.size();
int q2 = queue2.size();
for (int i = 0; i < q1; i++)
{
list2.add(queue1.dequeue());
}
for (int i = 0; i < q2; i++)
{
list2.add(queue2.dequeue());
}
list2.print();
delete[] ch;
return 0;
}

