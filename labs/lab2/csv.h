#include "list.h"

void   Csv_addInt               (List * row, int value);
void   Csv_addDouble            (List * row, double value);
void   Csv_addString            (List * row, const char * value);

void   Csv_addRow               (List * table, List * row);
List * Csv_row                  (List * table, int index); 

int    Csv_sizeOfString         (List *self);

void    Csv_fillTableFromString  (List * csvTable, const char * csvString); 
int    Csv_fillStringFromTable  (List * csvTable, char * buf); 
char * Csv_createStringFromTable(List * csvTable); 
void   Csv_clearTable           (List * csvTable);

void   Csv_printTable           (List *self);
