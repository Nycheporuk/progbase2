#include "list.h"
#include "csv.h"

int main(int argc, char *argv[])
{
    struct Ranobe testList[6] = {
        {"ranobe_fisrt ", "author_fisrt ", 1, "read    "},
        {"ranobe_second", "author_second", 2, "reading "},
        {"ranobe_third ", "author_third ", 3, "pause   "},
        {"ranobe_fourth", "author_fourth", 4, "non read"},
        {"ranobe_fifth ", "author_fifth ", 5, "read    "},
    };

    char name[50];
    char file[50];
    char ranobe[50];
    char str[1000];
    char num[5];

    file[0] = '\0';
    ranobe[0] = '\0';
    str[0] = '\0';
    name[0] = '\0';

    int ind = 0;
    int opt;
    int n = -1;
    while ((opt = getopt(argc, argv, "n:o:")) != -1)
    {
        switch (opt)
        {
        case 'n':
            n = atoi(optarg);
            break;
        case 'o':
            printf("option: %c, value: %s\n", opt, optarg);
            strcpy(file, optarg);
            break;
        case ':':
            printf("option needs a value\n");
            break;
        case '?':
            printf("unknown option: %c\n", optopt);
            break;
        }
    }

    if (optind < argc)
    {
        strcpy(name, argv[optind]);

        FILE *p = fopen(name, "r");
        if (p == NULL)
        {
            printf("error. can`t open:    %s\n", argv[optind]);
            exit(EXIT_FAILURE);
        }
        char ch = getc(p);
        while (ch != EOF)
        {
            str[ind++] = ch;
            ch = getc(p);
        }
        str[ind] = '\0';
        fclose(p);
    }
    else
    {
        for (int i = 0; i < sizeof(testList) / sizeof(testList[0]) - 1; i++)
        {
            strcat(str, testList[i].name);
            strcat(str, ",");
            strcat(str, testList[i].author);
            strcat(str, ",");
            sprintf(num, "%i", testList[i].numOfChapters);
            strcat(str, num);
            strcat(str, ",");
            strcat(str, testList[i].status);
            strcat(str, "\n");
        }
    }

    List *table = List_alloc();
    List *items = NULL;

    Csv_fillTableFromString(table, str);
   
    items = createListFromTable(table);

    if (n != -1)
    {
        processItems(items, n);
        processCSV(table, n);
    }
     printf("Table:\n");
    printf("===============================================\n");
    Csv_printTable(table);
    printf("===============================================\n");


    printf("\nList:\n");
    printf("===============================================");
    List_print(items);
    printf("===============================================\n");

    if (strlen(file) > 0)
    {
        FILE *pfile = fopen(file, "w");

        if (pfile == NULL)
        {
            printf("can`t open file %s \n", file);
            exit(EXIT_FAILURE);
        }
        items = List_toTable(items);
        char * str = Csv_createStringFromTable(items);
        fprintf(pfile, "%s", str);
        free(str);
        Csv_printTable(items);
        fclose(pfile);
    }

    if (strlen(file) > 0)
    {
        Csv_clearTable(items);
    }
    else
    {
        List_clearStruct(items);
        List_free(items);
    }
    Csv_clearTable(table);

    return 0;
}