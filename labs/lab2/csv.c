#include "csv.h"
#include "list.h"

void Csv_addInt(List *row, int value)
{
    if (row->size == row->capacity)
    {
        List_realloc(row);
    }

    char a[1];
    sprintf(a, "%d", value);
    row->items[row->size++] = strOnHeap(a);
}

void Csv_addDouble(List *row, double value)
{
    if (row->size == row->capacity)
    {
        List_realloc(row);
    }

    char a[1];
    sprintf(a, "%.2f", value);
    row->items[row->size++] = strOnHeap(a);
}

void Csv_addString(List *row, const char *value)
{
    if (row->size == row->capacity)
    {
        List_realloc(row);
    }
    char *p = malloc(sizeof(char *) * strlen(value));
    strcpy(p, value);
    row->items[row->size++] = p;
}

void Csv_addRow(List *table, List *row)
{
    List_add(table, row);
}

List *Csv_row(List *table, int index)
{
    return table->items[index];
}

int Csv_fillStringFromTable(List *csvTable, char *buf)
{
    buf[0] = '\0';
    for (int i = 0; i < List_size(csvTable); i++)
    {
        List *rowItem = List_get(csvTable, i);
        for (int j = 0; j < List_size(rowItem); j++)
        {
            char *value = List_get(rowItem, j);
            strcat(buf, value);
            if (j != List_size(rowItem) - 1)
            {
                strcat(buf, ",");
            }
        }
        if (i != List_size(csvTable) - 1)
        {
            strcat(buf, "\n");
        }
    }
    return 0;
}

char *Csv_createStringFromTable(List *csvTable)
{
    char *str = malloc(sizeof(char *) * Csv_sizeOfString(csvTable) - 1);
    str[0] = '\0';

    for (int i = 0; i < List_size(csvTable); i++)
    {
        List *rowItem = List_get(csvTable, i);

        for (int j = 0; j < List_size(rowItem); j++)
        {
            char *item = List_get(rowItem, j);

            strcat(str, item);
            if (j != List_size(rowItem) - 1)
            {
                strcat(str, ",");
            }
        }
        if (i != List_size(rowItem))
        {
            strcat(str, "\n");
        }
    }
    return str;
}
void Csv_fillTableFromString(List *csvTable, const char *csvString)
{
    char buf[100];
    int bufX = 0;

    List *list1 = NULL;

    while (true)
    {
        if (*csvString == ',')
        {
            if (list1 == NULL)
            {
                list1 = List_alloc();
            }
            buf[bufX] = '\0';
            bufX = 0;
            Csv_addRow(list1, strOnHeap(&buf[0]));
        }
        else if (*csvString == '\n')
        {
            if (bufX != 0)
            {
                buf[bufX] = '\0';
                bufX = 0;
                Csv_addRow(list1, strOnHeap(&buf[0]));
                Csv_addRow(csvTable, list1);
            }

            list1 = NULL;
        }
        else if (*csvString == '\0')
        {
            if (bufX == 0)
            {
                break;
            }
            if (list1 == NULL)
            {
                list1 = List_alloc();
            }

            buf[bufX] = '\0';
            Csv_addRow(list1, strOnHeap(&buf[0]));
            Csv_addRow(csvTable, list1);
            break;
        }
        else
        {
            buf[bufX++] = *csvString;
        }

        csvString += 1;
    }
}

int Csv_sizeOfString(List *self)
{
    int size = 0;

    for (int i = 0; i < List_size(self); i++)
    {
        List *rowItem = List_get(self, i);

        for (int j = 0; j < List_size(rowItem); j++)
        {
            char *item = List_get(rowItem, j);

            while (*item != '\0')
            {
                size += 1;
                item += 1;
            }
        }
    }

    return size;
}

void Csv_clearTable(List *csvTable)
{
    for (int i = 0; i < List_size(csvTable); i++)
    {
        void *l = List_get(csvTable, i);

        for (int j = 0; j < List_size(l1); j++)
        {
            char *str = List_get(l, j);
            free(str);
        }
        List_free(l);
    }
    List_free(csvTable);
}

void Csv_printTable(List *table)
{
    for (int i = 0; i < List_size(table); i++)
    {
        List *list = Csv_row(table, i);
        List_printCSV(list);
    }
}


