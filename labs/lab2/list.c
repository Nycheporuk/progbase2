#include "list.h"
#include "csv.h"
void List_init(List *self)
{

    self->capacity = 4;
    if ((self->items = malloc(sizeof(void *) * self->capacity)) == NULL)
    {
        printf("=error= can`t init\n");
        exit(1);
    }
    self->size = 0;
}
List *List_alloc()
{
    List *list;
    if ((list = malloc(sizeof(List))) == NULL)
    {
        printf("=error= can`t alloc\n");
        exit(1);
    }
    List_init(list);
    return list;
}
void List_realloc(List *self)
{
    self->capacity *= 2;
    void **reallocated = realloc(self->items, sizeof(char *) * self->capacity);
    self->items = reallocated;
}

void List_deinit(List *self)
{
    free(self->items);
}

void List_free(List *self)
{
    List_deinit(self);
    free(self);
}

void *List_get(List *self, int index)
{
    return self->items[index];
}

void List_set(List *self, int index, void *value)
{
    self->items[index] = value;
}

size_t List_size(List *self)
{
    return self->size;
}

void List_insert(List *self, int index, void *value)
{
    if (self->capacity == self->size)
    {
        List_realloc(self);
    }

    for (int i = self->size; i > index; i--)
    {
        self->items[i] = self->items[i - 1];
    }
    self->items[index] = value;
    self->size += 1;
}

void List_removeAt(List *self, int index)
{
    char *str = List_get(self, index);
    for (int i = index; i < List_size(self) - 1; i++)
    {
        self->items[i] = self->items[i + 1];
    }
    free(str);
    self->size -= 1;
}

void List_add(List *self, void *value)
{
    if (self->size == self->capacity)
    {
        List_realloc(self);
    }
    self->items[self->size++] = value;
}

void List_remove(List *self, void *value)
{
    for (int i = 0; i < List_size(self); i++)
    {
        if (self->items[i] == value)
        {
            List_removeAt(self, i);
            return;
        }
    }
}

int List_indexOf(List *self, void *value)
{
    for (int i = 0; i < List_size(self); i++)
    {
        if (self->items[i] == value)
        {
            return i;
        }
    }
    return -1;
}

bool List_contains(List *self, void *value)
{
    for (int i = 0; i < List_size(self); i++)
    {
        if (self->items[i] == value)
        {
            return true;
        }
    }
    return false;
}

bool List_isEmpty(List *self)
{
    if (self->size == 0)
        return true;

    return false;
}

void *strOnHeap(void *str)
{
    char *string = malloc(strlen(str) + 1);
    strcpy(string, str);
    return string;
}

void List_printCSV(List *self)
{
    for (int i = 0; i < List_size(self); i++)
    {
        printf("%s", (char *)List_get(self, i));
        if (i < List_size(self) - 1)
        {
            printf(", ");
        }
    }
    printf("\n");
}

List *createListFromTable(List *csvTable)
{
    List *list2 = List_alloc();
    struct Ranobe *ranobe = NULL;
    for (int i = 0; i < List_size(csvTable); i++)
    {
        List *list = List_get(csvTable, i);
        ranobe = malloc(sizeof(struct Ranobe));
        ranobe->name = (char *)List_get(list, 0);
        ranobe->author = List_get(list, 1);
        char *ch = (char *)List_get(list, 2);
        ranobe->numOfChapters = atoi(ch);
        ranobe->status = (char *)List_get(list, 3);
        List_add(list2, ranobe);
    }
    return list2;
}
void List_valueFromListToStr(List *self, char *str)
{
    str[0] = '\0';
    for (int i = 0; i < self->size; i++)
    {
        struct Ranobe *ranobe = self->items[i];
        strcat(str, ranobe->name);
        strcat(str, ",");
        strcat(str, ranobe->author);
        strcat(str, ",");
        char numOfChapters[10];
        sprintf(numOfChapters, "%d", ranobe->numOfChapters);
        strcat(str, numOfChapters);
        strcat(str, ",");
        strcat(str, ranobe->status);
        strcat(str, "\n");
    }
}



List *List_toTable(List *self)
{
    char str[1000];
    List_valueFromListToStr(self, str);
    for (int i = 0; i < List_size(self); i++)
    {
        List *list = List_get(self, i);
        free(list);
    }
    List_deinit(self);
    List_init(self);
    Csv_fillTableFromString(self, str);
    return self;
}

void List_print(List *self)
{
    printf("\n");
    for (int i = 0; i < List_size(self); i++)
    {
        struct Ranobe *ranobe = List_get(self, i);
        printf("%s, %s, %d, %s\n", ranobe->name, ranobe->author, ranobe->numOfChapters, ranobe->status);
    }
}

void List_clearStruct(List *self)
{
    for (int i = 0; i < List_size(self); i++)
    {
        List *l = List_get(self, i);
        free(l);
    }
}